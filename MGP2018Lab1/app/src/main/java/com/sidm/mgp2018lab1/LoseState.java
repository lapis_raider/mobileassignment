package com.sidm.mgp2018lab1;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.util.DisplayMetrics;
import android.view.SurfaceView;

import com.sidm.mgp2018lab1.EntityObjects.RenderBackground;
import com.sidm.mgp2018lab1.EntityObjects.UI.CreateEmoji;
import com.sidm.mgp2018lab1.EntityObjects.UI.MenuButton;
import com.sidm.mgp2018lab1.EntityObjects.UI.ScoreText;
import com.sidm.mgp2018lab1.EntityObjects.UI.ScoreTextPanel;
import com.sidm.mgp2018lab1.EntityObjects.UI.SpeechBubble;
import com.sidm.mgp2018lab1.grid.GameGrid;
import com.sidm.mgp2018lab1.grid.Grid;
import com.sidm.mgp2018lab1.grid.GridPt;

import java.util.Vector;

public class LoseState implements StateBase{
    int ScreenWidth, ScreenHeight;

    // Game map
    private GameGrid gameGrid;

    private boolean isPaused = false;

    @Override
    public String GetName() { return "LoseState"; }

    @Override
    public void OnEnter(SurfaceView _view)
    {
        DisplayMetrics metrics = _view.getResources().getDisplayMetrics();
        ScreenWidth = metrics.widthPixels;
        ScreenHeight = metrics.heightPixels;

        EntityManager.Instance.Init(_view);

        // Creation of map object
        Grid.TILE_CONTENT[] array = new Grid.TILE_CONTENT[10 * 5];
        for (int i = 0; i < 10 * 5; ++i)
            array[i] = Grid.TILE_CONTENT.TILE_EMPTY;
        // Set entity pos in Grid here use gameGrid.GridPtIndex(...) to get index to place
        gameGrid = new GameGrid(10, 5, ScreenWidth, ScreenHeight); //create the map

        gameGrid.SetGrid(array);
        Player.Instance.Create(new GridPt(4, 2), gameGrid);
        count=0;
    }

    @Override
    public void OnExit() {
        EntityManager.Instance.Destroy();
    }

    @Override
    public void Render(Canvas _canvas)
    {
        EntityManager.Instance.Render(_canvas); //cause entity manager have their own canvas to render out
    }

    public float count = 0;
    @Override
    public void Update(float _dt) {
        if (count == 0) //must be called after init once
        {
            // Create text bubble
            MenuButton.Create();
            SpeechBubble.Create("We wasted too much\nmoney this month!\nWe can only eat cup\nnoodles next month!");
            RenderBackground.Create();
            ScoreText.Create();
            ScoreTextPanel.Create();
            CreateEmoji.Create("sad");
            ++count;
        }

        EntityManager.Instance.Update(_dt);
    }
}
