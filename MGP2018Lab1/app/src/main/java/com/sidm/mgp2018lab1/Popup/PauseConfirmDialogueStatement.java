package com.sidm.mgp2018lab1.Popup;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

import com.sidm.mgp2018lab1.GameSystem;
import com.sidm.mgp2018lab1.Player;
import com.sidm.mgp2018lab1.Samplegame;
import com.sidm.mgp2018lab1.TouchManager;

public class PauseConfirmDialogueStatement extends DialogFragment {
    public static boolean IsShown = false; //to indicate false to builder

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState)
    {
        IsShown = true;

        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("Go back to main menu?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User triggered pause
                        GameSystem.Instance.SetIsPaused(false);
                        Player.Instance.changeScene = true;
                        IsShown = false;
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the pause
                        GameSystem.Instance.SetIsPaused(false);
                        Player.Instance.changeScene = false;
                        IsShown = false;
                    }
                });
        // Create the AlertDialog object and return it
        return builder.create();
    }

    @Override
    public void onCancel(DialogInterface dialog)
    {
        GameSystem.Instance.SetIsPaused(false);
        Player.Instance.changeScene = false;
        IsShown = false;
    }

    @Override
    public void onDismiss(DialogInterface dialog)
    {
        GameSystem.Instance.SetIsPaused(false);
        Player.Instance.changeScene = false;
        IsShown = false;
    }
}
