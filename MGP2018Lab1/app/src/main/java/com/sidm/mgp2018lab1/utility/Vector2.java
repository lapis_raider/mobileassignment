package com.sidm.mgp2018lab1.utility;

import java.lang.Math;

public class Vector2 {

    public float x, y;

    // Constructor
    public Vector2() { x = 0; y = 0; }
    public Vector2(float _x, float _y ) { x = _x; y = _y; }
    public Vector2(Vector2 RHS) { x = RHS.x; y = RHS.y; }

    // Operators
    public Vector2 Add(Vector2 RHS) { return new Vector2(x + RHS.x, y + RHS.y); }
    public Vector2 Minus(Vector2 RHS) { return new Vector2(x - RHS.x, y - RHS.y);}
    public Vector2 Multiply(Vector2 RHS) { return new Vector2(x * RHS.x, y * RHS.y);}
    public float Dot(Vector2 RHS) { return x * RHS.x + y * RHS.y; }
    public float Length() { return (float)Math.sqrt( x * x + y * y ); }
    public float LengthSquared() { return x * x + y * y; }
    public Boolean isEqual(Vector2 RHS) { return Math.abs(x - RHS.x) <= 1 && Math.abs(y - RHS.y) <= 1; }

    public void Set(float _x, float _y) { x = _x; y = _y; }
}
