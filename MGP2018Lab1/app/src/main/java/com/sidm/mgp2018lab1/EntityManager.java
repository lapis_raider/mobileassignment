package com.sidm.mgp2018lab1;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.provider.ContactsContract;
import android.view.SurfaceView;

import com.sidm.mgp2018lab1.grid.GameGrid;
import com.sidm.mgp2018lab1.grid.GridPt;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.Vector;

public class EntityManager
{
    public final static EntityManager Instance = new EntityManager();
    private LinkedList<EntityBase> entityList = new LinkedList<EntityBase>(); //can use hash map if needed too
    private Vector<EntityBase>  itemQueue = new Vector<>();
    private SurfaceView view = null;

    private EntityManager()
    {
    }

    public void Init(SurfaceView _view)
    {
        view = _view;
    }

    public void Update(float _dt)
    {
        LinkedList<EntityBase> removalList = new LinkedList<EntityBase>();
        // Reset first. Change later in checking
        Player.Instance.electricity =  Player.Instance.water = false;

        // Update all
        for (EntityBase currEntity : entityList)
        {
            if (currEntity.entityTypes != Collidable.EntityTypes.DEFAULT && GameSystem.Instance.GetIsPaused())
                continue;

            currEntity.Update(_dt);

            if (currEntity.GetIsActive())
            {
                if (currEntity.billType == Collidable.BillType.ELECTRIC)
                {
                    Player.Instance.electricity = true;
                }
                else if (currEntity.billType == Collidable.BillType.WATER)
                {
                    Player.Instance.water = true;
                }
            }

            // Check if need to clean up
            if (currEntity.IsDone())
                removalList.add(currEntity);
        }

        // Remove all entities that are done
        for (EntityBase currEntity : removalList)
            entityList.remove(currEntity);

        removalList.clear(); // Clean up of removal list

        //if need to create anything, create here
        while (!itemQueue.isEmpty())
        {
            AddEntity(itemQueue.firstElement()); //add into entity manager
            itemQueue.remove(itemQueue.firstElement()); //remove from queue
        }

        // Collision Check
        for (int i = 0; i < entityList.size(); ++i)
        {
            EntityBase currEntity = entityList.get(i);

            // Check if need to clean up
            if (currEntity.IsDone())
                removalList.add(currEntity);
        }

        // Remove all entities that are done
        for (EntityBase currEntity : removalList)
            entityList.remove(currEntity);
    }

    public void Render(Canvas _canvas)
    {
        //use the new rendering layer to sort the render layer
        Collections.sort(entityList, new Comparator<EntityBase>()
        {
            @Override
            public int compare(EntityBase o1, EntityBase o2)
            {
                return o1.GetRenderLayer() - o2.GetRenderLayer();
            }
        });


        for (EntityBase currEntity : entityList)
            currEntity.Render(_canvas);
    }

    public void AddEntity(EntityBase _newEntity)
    {
        _newEntity.Init(view);
        entityList.add(_newEntity);
    }

    public void AddEntity(EntityBase _newEntity, GridPt grid, GameGrid gridData)
    {
        _newEntity.Init(view, grid, gridData);
        entityList.add(_newEntity);
    }

    public void AddEntity(EntityBase _newEntity, String _text)
    {
        _newEntity.Init(view, _text);
        entityList.add(_newEntity);
    }

    public void AddQueue(EntityBase _newEntity)
    {
        itemQueue.add(_newEntity);
    }

    public LinkedList<EntityBase> GetEntityList()
    {
        return entityList;
    }

    public void Destroy() { entityList.clear(); }
}

