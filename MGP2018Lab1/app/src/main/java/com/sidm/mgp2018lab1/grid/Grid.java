package com.sidm.mgp2018lab1.grid;

import java.util.Vector;

public class Grid {

    public Grid(float _sizeX, float _sizeY, float _screenWidth, float _screenHeight)
    {
        m_sizeX = (int)(_sizeX);
        m_sizeY = (int)(_sizeY);
        m_gridSizeX = (int)(_screenWidth/_sizeX);
        m_gridSizeY = (int)(_screenHeight/_sizeY);
        m_gridOffsetX = m_gridSizeX/2;
        m_gridOffsetY = m_gridSizeY/2;

        m_curr = new GridPt();
        m_grid = new Vector<Grid.TILE_CONTENT>();
    }

    public Boolean SetGrid(Grid.TILE_CONTENT _grid[])
    {
        //Do stuff to create grid map here
        // 10 x 5
        // [41] [42] [43] [44] [45] [46] [47] [48] [49] [50]
        // [31] [32] [33] [34] [35] [36] [37] [38] [39] [40]
        // [21] [22] [23] [24] [25] [26] [27] [28] [29] [20]
        // [11] [12] [13] [14] [15] [16] [17] [18] [19] [20]
        // [01] [02] [03] [04] [05] [06] [07] [08] [09] [10]
        //m_grid

        for (Grid.TILE_CONTENT grid : _grid)
            m_grid.add(grid);

        return false;
    }

    public enum TILE_CONTENT
    {
        TILE_EMPTY,
        TILE_WALL,
        TILE_OBJECT,
    }

    public GridPt GetCurr()
    {
        return m_curr;
    }

    public void SetCurr(GridPt newCurr)
    {
        m_curr = newCurr;
    }

    ///////////////////////
    // Grid Data Section //
    ///////////////////////
    // Template grid size is 10x5, width being 185px, height being 213px
    // Stick to 10x5 for assignment, but value can be changed when needed
    public int m_gridSizeX, m_gridSizeY; // Size of each grid in x and y axis
    public float m_gridOffsetX, m_gridOffsetY; // Size of each grid's offset to center in x and y axis
    public int m_sizeX, m_sizeY; // Num of grid in x and y axis

    private GridPt m_curr;
    public Vector<Grid.TILE_CONTENT> m_grid;
}
