package com.sidm.mgp2018lab1;


import android.media.MediaPlayer;
import android.view.SurfaceView;

import java.util.HashMap;

public class AudioManager {
    public final static AudioManager Instance = new AudioManager();

    private SurfaceView view = null;
    private float volumeLevel = 1.f;
    private HashMap<Integer, MediaPlayer> audioMap = new HashMap<Integer, MediaPlayer>();


    private AudioManager()
    {

    }

    public void Init(SurfaceView _view)
    {
        view = _view;
        Exit();
    }

    //adjust sound from 0.f to 1.f
    public void PlayAudio(int _id, float _vol)
    {
        if (audioMap.containsKey(_id))
        {
            float vol = _vol * volumeLevel;
            MediaPlayer curr = audioMap.get(_id);
            curr.seekTo(0);
            curr.setVolume(vol, vol);
            curr.start();
        }
        else //put the audio into the audio map if its not in the audio map yet
        {
            float vol = _vol * volumeLevel;
            MediaPlayer newAudio = MediaPlayer.create(view.getContext(), _id);
            audioMap.put(_id, newAudio);
            newAudio.setVolume(vol, vol);
            newAudio.start(); //just play the audio immediately
        }
    }

    public void Exit()
    {
        for (HashMap.Entry<Integer, MediaPlayer> entry : audioMap.entrySet())
        {
            //for hashmap, similar to vector functions
            entry.getValue().stop();
            entry.getValue().reset();
            entry.getValue().release();
        }
        audioMap.clear();
    }

    public void StopAudio(int _id)
    {
        MediaPlayer Audio = audioMap.get(_id);
        Audio.pause();
    }

    public void SetAudioLevel(float audioLevel)
    {
        volumeLevel = audioLevel;
    }

    public float GetAudioLevel()
    {
        return volumeLevel;
    }
}
