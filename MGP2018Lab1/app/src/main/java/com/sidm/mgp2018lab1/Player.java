package com.sidm.mgp2018lab1;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.os.Build;
import android.os.VibrationEffect;
import android.util.Log;
import android.view.SurfaceView;

import com.sidm.mgp2018lab1.grid.GameGrid;
import com.sidm.mgp2018lab1.grid.Grid;
import com.sidm.mgp2018lab1.grid.GridPt;
import com.sidm.mgp2018lab1.utility.Vector2;

import java.util.Vector;

import static android.support.constraint.Constraints.TAG;
import static com.sidm.mgp2018lab1.Player.MovingDir.MOVEDOWN;
import static com.sidm.mgp2018lab1.Player.MovingDir.MOVELEFT;
import static com.sidm.mgp2018lab1.Player.MovingDir.MOVERIGHT;
import static com.sidm.mgp2018lab1.Player.MovingDir.MOVEUP;
import static com.sidm.mgp2018lab1.Player.MovingDir.NONE;
import android.os.Vibrator;

public class Player extends EntityBase{

    public final static Player Instance = new Player();

    //spritesheets
    Sprite leftWalkingAnim = null;
    Sprite rightWalkingAnim =null;
    Sprite upWalkingAnim = null;
    Sprite downWalkingAnim = null;

    Bitmap leftStaticSprite = null;
    Bitmap rightStateSprite = null;
    Bitmap downStaticSprite = null;
    Bitmap upStaticSprite = null;

    GridPt gridTarget;
    Vector2 target;

    Vector<GridPt> m_shortest = new Vector<>();

    boolean isMoving;

    float penaltyTime;
    public boolean busy;

    public GameGrid gameData;

    public float elecBill;
    public float waterBill;

    // Store so I dont have to keep reusing GameGrid to pass
    float m_gridSizeX, m_gridSizeY;
    float m_gridOffsetX, m_gridOffsetY;

    public boolean changeScene = false;
    public boolean optionControl = false; //false for Dpad, true for clicking
    public boolean interactButton = false;
    public boolean electricity = false; // Keep track of anything being used, used for UI panel class
    public boolean water = false;  // Keep track of anything being used, used for UI panel class

    public Vibrator m_vibrator;

    float END_TIME = 180; //3 mins
    private float gameTimer = END_TIME; //timer to make it

    public int HIGHSCORE_KEEP = 5; //number of highscore to keep track of

    public float lastBill;

    public enum MovingDir
    {
        NONE,
        MOVERIGHT,
        MOVELEFT,
        MOVEUP,
        MOVEDOWN
    }
    private MovingDir dirMoved = NONE;

    @Override
    public void Init(SurfaceView _view, GridPt grid, GameGrid gridData)
    {
        waterBill = 0;
        elecBill = 0;

        gameData = gridData;

        m_gridSizeX = gridData.grid.m_gridSizeX;
        m_gridSizeY = gridData.grid.m_gridSizeY;
        m_gridOffsetX = gridData.grid.m_gridOffsetX;
        m_gridOffsetY= gridData.grid.m_gridOffsetY;

        gameTimer = END_TIME;

        //initialise all the sprite animation and stuff here
        leftStaticSprite = BitmapFactory.decodeResource(_view.getResources(), R.drawable.walkstaticleft);
        upStaticSprite = BitmapFactory.decodeResource(_view.getResources(), R.drawable.walkstaticup);
        downStaticSprite = BitmapFactory.decodeResource(_view.getResources(), R.drawable.walkstaticdown);
        rightStateSprite = BitmapFactory.decodeResource(_view.getResources(), R.drawable.walkstaticright);

        spritePic = downStaticSprite;
        Scale(2.f,2.f); //change Size

        //change sizes here
        leftStaticSprite = Bitmap.createScaledBitmap(leftStaticSprite, (int)sizeWidth, (int)sizeHeight, true);
        upStaticSprite = Bitmap.createScaledBitmap(upStaticSprite, (int)sizeWidth, (int)sizeHeight, true);
        downStaticSprite = Bitmap.createScaledBitmap(downStaticSprite, (int)sizeWidth, (int)sizeHeight, true);
        rightStateSprite = Bitmap.createScaledBitmap(rightStateSprite, (int)sizeWidth, (int)sizeHeight, true);

        leftWalkingAnim = new Sprite(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(_view.getResources(), R.drawable.walkanimationleft), (int)(sizeWidth), (int)(sizeHeight), true),1,3, 1 );
        rightWalkingAnim = new Sprite(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(_view.getResources(), R.drawable.walkanimationright), (int)(sizeWidth), (int)(sizeHeight), true),1,3, 1 );
        upWalkingAnim = new Sprite(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(_view.getResources(), R.drawable.walkanimationup), (int)(sizeWidth ), (int)(sizeHeight), true),1,3, 1 );
        downWalkingAnim = new Sprite(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(_view.getResources(), R.drawable.walkanimationdown), (int)(sizeWidth ), (int)(sizeHeight), true),1,3, 1 );

        animation = downWalkingAnim;

        //set positions
        gridPos = new GridPt(grid.x, grid.y);
        gridTarget = gridPos;
        position = new Vector2(gridPos.x * m_gridSizeX + m_gridOffsetX , ( gameData.grid.m_sizeY - 1 - gridPos.y) * m_gridSizeY + m_gridOffsetY);

        target = position;
        isMoving = false;

        // Set up vibrator
        m_vibrator = (Vibrator) _view.getContext().getSystemService(_view.getContext().VIBRATOR_SERVICE);
        entityTypes = EntityTypes.MOVING_ENTITIES;
    }

    public void Exit()
    {
        waterBill = 0;
        elecBill = 0;

        gameData = null;

        m_gridSizeX = 0;
        m_gridSizeY = 0;
        m_gridOffsetX = 0;
        m_gridOffsetY= 0;

        //initialise all the sprite animation and stuff here
        leftStaticSprite = null;
        upStaticSprite = null;
        downStaticSprite = null;
        rightStateSprite = null;

        spritePic = null;

        //change sizes here
        leftStaticSprite = null;
        upStaticSprite = null;
        downStaticSprite = null;
        rightStateSprite = null;

        leftWalkingAnim = null;
        rightWalkingAnim = null;
        upWalkingAnim = null;
        downWalkingAnim = null;

        animation = null;

        //set positions
        gridPos = null;
        gridTarget = null;
        position = null;
        dirMoved = NONE;

        target = null;
        isMoving = false;
        m_shortest.clear();
        m_vibrator = null;

        gameTimer = END_TIME;
    }

    @Override
    public void Update(float _dt)
    {
        if (StateManager.Instance.GetCurrentState() != "LoseState" && StateManager.Instance.GetCurrentState() != "WinState")
        {
            if (gameTimer <= 0) {

                int value = -1;
                int currentSave = -1;
                Boolean stored = false;
                GameSystem.Instance.SaveEditBegin();
                for (int i =0; i < HIGHSCORE_KEEP; ++i)
                {
                    String name = "SAVE" + i;

                    if (GameSystem.Instance.GetIntFromSave(name) == 0)
                    {
                        GameSystem.Instance.SetIntInSave(name, (int)(elecBill + waterBill)); //once stored break
                        currentSave = -1;//reset the value back
                        break; //stop the loop once theres something saved
                    }
                    else
                    {
                        if (currentSave == -1) //if its first round
                        {
                            value = GameSystem.Instance.GetIntFromSave(name); //GET value
                            currentSave = i;
                        }
                        else
                        {
                            if (GameSystem.Instance.GetIntFromSave(name) > value) //the smaller value is better, replace the value
                            {
                                value = GameSystem.Instance.GetIntFromSave(name);
                                currentSave = i;
                            }
                        }
                    }
                }

                if (currentSave != -1) //if theres a save, replace
                {
                    String name = "SAVE" + currentSave;
                    GameSystem.Instance.SetIntInSave(name, (int)(elecBill + waterBill));
                }

                GameSystem.Instance.SaveEditEnd();

                lastBill = elecBill + waterBill;

                if (lastBill > 100)
                    StateManager.Instance.ChangeState("LoseState"); // Lose too much money
                else
                    StateManager.Instance.ChangeState("WinState"); // Save enough money
            } else {
                gameTimer -= _dt;
            }
        }

        if (penaltyTime > 0)
            penaltyTime -= _dt;
        else
            busy = false;

        if (optionControl) {
            if (isMoving) {
                animation.Update(_dt); //update animation too
                // When pos is not the same as target
                if (dirMoved != NONE) {
                    if (dirMoved == MOVEDOWN && position.y < target.y)
                        position.y += _dt * 200;
                    else if (dirMoved == MOVEDOWN && position.y >= target.y) {
                        spritePic = downStaticSprite;
                        dirMoved = NONE;
                    }

                    if (dirMoved == MOVEUP && position.y > target.y)
                        position.y -= _dt * 200;
                    else if (dirMoved == MOVEUP && position.y <= target.y) {
                        spritePic = upStaticSprite;
                        dirMoved = NONE;
                    }

                    if (dirMoved == MOVERIGHT && position.x < target.x)
                        position.x += _dt * 200;
                    else if (dirMoved == MOVERIGHT && position.x >= target.x) {
                        spritePic = rightStateSprite;
                        dirMoved = NONE;
                    }

                    if (dirMoved == MOVELEFT && position.x > target.x)
                        position.x -= _dt * 200;
                    else if (dirMoved == MOVELEFT && position.x <= target.x) {
                        spritePic = leftStaticSprite;
                        dirMoved = NONE;
                    }
                } else {
                    position = new Vector2(target.x, target.y);
                    gridPos = gridTarget;
                    m_shortest.remove(0);

                    if (m_shortest.size() != 0) {
                        if (m_shortest.get(0).x - gridTarget.x < 0) {
                            animation = leftWalkingAnim;
                            dirMoved = MOVELEFT;
                        } else if (m_shortest.get(0).x - gridTarget.x > 0) {
                            animation = rightWalkingAnim;
                            dirMoved = MOVERIGHT;
                        } else if (m_shortest.get(0).y - gridTarget.y < 0) {
                            animation = downWalkingAnim;
                            dirMoved = MOVEDOWN;
                        } else if (m_shortest.get(0).y - gridTarget.y > 0) {
                            animation = upWalkingAnim;
                            dirMoved = MOVEUP;
                        } else
                            dirMoved = NONE;

                        target = new Vector2(m_shortest.get(0).x * m_gridSizeX + m_gridOffsetX, (gameData.grid.m_sizeY - 1 - m_shortest.get(0).y) * m_gridSizeY + m_gridOffsetY);
                        gridTarget.Set(m_shortest.get(0).x, m_shortest.get(0).y);
                    } else {
                        isMoving = false;
                    }

                }
            }
        }
        else {
            if (isMoving) {
                animation.Update(_dt); //update animation too
                if (dirMoved != NONE) {
                    if (dirMoved == MOVEDOWN && position.y < target.y)
                        position.y += _dt * 200;
                    else if (dirMoved == MOVEDOWN && position.y > target.y) {
                        spritePic = downStaticSprite;
                        dirMoved = NONE;
                    }

                    if (dirMoved == MOVEUP && position.y > target.y)
                        position.y -= _dt * 200;
                    else if (dirMoved == MOVEUP && position.y < target.y) {
                        spritePic = upStaticSprite;
                        dirMoved = NONE;
                    }

                    if (dirMoved == MOVERIGHT && position.x < target.x)
                        position.x += _dt * 200;
                    else if (dirMoved == MOVERIGHT && position.x > target.x) {
                        spritePic = rightStateSprite;
                        dirMoved = NONE;
                    }

                    if (dirMoved == MOVELEFT && position.x > target.x)
                        position.x -= _dt * 200;
                    else if (dirMoved == MOVELEFT && position.x < target.x) {
                        spritePic = leftStaticSprite;
                        dirMoved = NONE;
                    }
                } else {
                    target.Set(gridTarget.x * m_gridSizeX + m_gridOffsetX, (gameData.grid.m_sizeY - 1 - gridTarget.y) * m_gridSizeY + m_gridOffsetY);
                    position = target;
                    gridPos = gridTarget;
                    isMoving = false;
                }
            }
        }
        CheckCollision();
    }

    @Override
    public void Render(Canvas _canvas)
    {
        if (isMoving) //if moving do the animation
            animation.Render(_canvas, (int)position.x, (int)position.y);
        else
            _canvas.drawBitmap(spritePic, null, new Rect((int)(position.x - sizeWidth * 0.5f), (int)(position.y - sizeHeight * 0.5f), (int)(position.x + sizeWidth * 0.5f), (int)(position.y + sizeHeight * 0.5f)), null);
    }

    //set bfs
    public void SetPath (GameGrid gameData)
    {
        // Set a queue with the m_previous vector from gameGrid
        for ( int i = 0; i < gameData.m_shortestPath.size(); ++i)
        {
            m_shortest.add(gameData.m_shortestPath.get(i));
        }
        if (m_shortest.size() > 0)
        {
            if (m_shortest.get(0).x - gridTarget.x < 0)
            {
                animation = leftWalkingAnim;
                dirMoved = MOVELEFT;
            }
            else if (m_shortest.get(0).x - gridTarget.x > 0)
            {
                animation = rightWalkingAnim;
                dirMoved = MOVERIGHT;
            }
            else if (m_shortest.get(0).y - gridTarget.y < 0)
            {
                animation = downWalkingAnim;
                dirMoved = MOVEDOWN;
            }
            else if (m_shortest.get(0).y - gridTarget.y > 0)
            {
                animation = upWalkingAnim;
                dirMoved = MOVEUP;
            }
            else
                dirMoved = NONE;

            target.Set(m_shortest.get(0).x * m_gridSizeX + m_gridOffsetX, (gameData.grid.m_sizeY - 1 - m_shortest.get(0).y) * m_gridSizeY + m_gridOffsetY);
            //target.Set
            isMoving = true;
        }
    }

    public void SetDir(MovingDir dirMovingTo) //pressing buttons to move to certain directions
    {
        if (isMoving)
            return;

        switch (dirMovingTo)
        {
            case MOVEDOWN: {
                GridPt next = new GridPt(gridPos.x, gridPos.y - 1);
                if (next.y < 0)
                    break;
                if (gameData.grid.m_grid.get(gameData.GridPtIndex(next)) != Grid.TILE_CONTENT.TILE_EMPTY )
                    break;
                target = new Vector2(position.x, position.y + m_gridSizeY);
                gridTarget = new GridPt(gridPos.x, gridPos.y - 1);
                animation = downWalkingAnim;
                isMoving = true;
                dirMoved = dirMovingTo;
                break;
            }
            case MOVEUP: {
                GridPt next = new GridPt(gridPos.x, gridPos.y + 1);
                if ( next.y >= gameData.grid.m_sizeY)
                    break;
                if (gameData.grid.m_grid.get(gameData.GridPtIndex(next)) != Grid.TILE_CONTENT.TILE_EMPTY )
                    break;
                target = new Vector2(position.x, position.y - m_gridSizeY);
                gridTarget = new GridPt(gridTarget.x, gridTarget.y + 1);
                animation = upWalkingAnim;
                isMoving = true;
                dirMoved = dirMovingTo;
                break;
            }
            case MOVELEFT: {
                GridPt next = new GridPt(gridPos.x - 1, gridPos.y);
                if (next.x < 0)
                    break;
                if (gameData.grid.m_grid.get(gameData.GridPtIndex(next)) != Grid.TILE_CONTENT.TILE_EMPTY)
                    break;
                target = new Vector2(position.x - m_gridSizeX, position.y);
                gridTarget = new GridPt(gridTarget.x - 1, gridTarget.y);
                animation = leftWalkingAnim;
                isMoving = true;
                dirMoved = dirMovingTo;
                break;
            }
            case MOVERIGHT: {
                GridPt next = new GridPt(gridPos.x + 1, gridPos.y);
                if (next.x >= gameData.grid.m_sizeX)
                    break;
                if (gameData.grid.m_grid.get(gameData.GridPtIndex(next)) != Grid.TILE_CONTENT.TILE_EMPTY )
                    break;
                target = new Vector2(position.x + m_gridSizeX, position.y);
                gridTarget = new GridPt(gridTarget.x + 1, gridTarget.y);
                animation = rightWalkingAnim;
                isMoving = true;
                dirMoved = dirMovingTo;
                break;
            }
        }
    }


    public void CheckCollision()
    {
        for (int i = 0; i < EntityManager.Instance.GetEntityList().size(); ++i)
        {
            EntityBase currEntity = EntityManager.Instance.GetEntityList().get(i);
            if (currEntity.hasCollider)
                Interact(currEntity);
        }
    }

    public void startVibrate()
    {
        long pattern[] = {0, 50, 0};

        if (Build.VERSION.SDK_INT >= 26)
        {
            m_vibrator.vibrate(VibrationEffect.createOneShot(150, 10));
        }
        else
        {
            m_vibrator.vibrate(pattern, -1);
            Log.v(TAG, "test if Vibration had occured");
        }
    }
    public void stopVibrate() {m_vibrator.cancel();}

    public void Interact(EntityBase obj)
    {
        if (busy)
            return;

        // This is for saving/reading data.
        //int currScore = GameSystem.Instance.GetIntFromSave("Score");
        //++currScore;
        //GameSystem.Instance.SaveEditBegin();
        //GameSystem.Instance.SetIntInSave("Score", currScore);
        //GameSystem.Instance.SaveEditEnd();

        if (obj.GetType() == EntityTypes.RUBBISH) {
            if (Collision.SphereToSphere(position.x, position.y, GetRadius(), obj.GetPos().x,obj.GetPos().y, obj.GetRadius() )) {
                obj.SetIsDone(true);
                AudioManager.Instance.PlayAudio(R.raw.rubbish, 0.2f); //to play audio
                startVibrate();

                waterBill -= 0.25f;
                elecBill -= 0.25f;

                if (waterBill < 0)
                    waterBill = 0;

                if (elecBill < 0)
                    elecBill = 0;
            }
        }
        else if (obj.GetType() != EntityTypes.DEFAULT && obj.GetType() != EntityTypes.RUBBISH) //check if player beside the TV
        {
            if (gridPos.isEqual(new GridPt(obj.gridPos.x -1, obj.gridPos.y)) || gridPos.isEqual(new GridPt(obj.gridPos.x + 1, obj.gridPos.y))  || gridPos.isEqual(new GridPt(obj.gridPos.x, obj.gridPos.y - 1)) || gridPos.isEqual(new GridPt(obj.gridPos.x, obj.gridPos.y + 1))) {
                if (interactButton && obj.GetIsActive()) {
                    obj.SetActive(false);
                    startVibrate();

                    if (obj.billType == BillType.WATER)
                    {
                        AudioManager.Instance.PlayAudio(R.raw.faucet, 1); //to play audio
                    }
                    else if (obj.billType == BillType.ELECTRIC)
                    {
                        AudioManager.Instance.PlayAudio(R.raw.power_down, 1); //to play audio
                    }

                    busy = true;
                    switch (obj.GetType())
                    {
                        case TELEVISION:
                            penaltyTime = 2;
                            break;
                        case COMPUTER:
                            penaltyTime = 4;
                            break;
                        case TAP:
                            penaltyTime = 1;
                            break;
                        case FRIDGE:
                            penaltyTime = 1;
                            break;
                    }
                }
            }
        }
    }

    @Override
    public float GetRadius()
    {
        return animation.GetWidth() /2;
    }

    @Override
    public Vector2 GetPosMin() {
        return new Vector2(position.x - animation.GetWidth()/2,position.y - animation.GetHeight()/2);
    }

    @Override
    public Vector2 GetPosMax()
    {
        return new Vector2(position.x + animation.GetWidth()/2 ,position.y + animation.GetHeight() /2);
    }

    public static Player Create(GridPt grid, GameGrid gridData)
    {
        EntityManager.Instance.AddEntity(Instance, grid, gridData);
        return Instance;
    }

    @Override
    public int GetRenderLayer()
    {
        return LayerConstants.MOVING_GAMEOBJECTS_LAYER;
    }

    @Override
    public void SetRenderLayer(int _newLayout) { return; }

    public boolean CheckTimeUp()
    {
        if (gameTimer <= 0.f)
            return true;

        return false;
    }

    public float GetTime()
    {
        return gameTimer;
    }


    public GridPt GetGridTarget()
    {
        return gridTarget;
    }
}
