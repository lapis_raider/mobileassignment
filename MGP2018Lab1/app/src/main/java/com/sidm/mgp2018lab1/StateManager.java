package com.sidm.mgp2018lab1;

import android.graphics.Canvas;
import android.view.SurfaceView;

import java.util.HashMap;

//manage different state of the game, like for pause button
public class StateManager {
    public static final  StateManager Instance = new StateManager();

    //container to store all our states
    private HashMap<String, StateBase> stateMap = new HashMap<>();
    private StateBase currState = null;
    private StateBase nextState = null;

    private SurfaceView view = null;

    private StateManager()
    {

    }

    public void Init(SurfaceView _view)
    {
        view = _view;
    }

    void AddState(StateBase _newState)
    {
        //add the state into the state map
        stateMap.put(_newState.GetName(), _newState);
    }

    public void ChangeState(String _nextState)
    {
        //try to assign the next state
        nextState = stateMap.get(_nextState);

        //if no next state we assign back to current state
        if (nextState == null)
            nextState = currState;

        //extra to add if possible, throw away some warning if next state function fails
    }

    void Update(float _dt)
    {
        if (nextState != currState)
        {
            //we need change state
            currState.OnExit();
            nextState.OnEnter(view);
            currState = nextState;
        }

        if (currState == null)
            return;

        currState.Update(_dt);
    }

    void Render(Canvas _canvas)
    {
        currState.Render(_canvas);
    }

    public String GetCurrentState()
    {
        if (currState == null)
            return "INVALID";

        return currState.GetName();
    }

    public void Start(String _newCurrent)
    {
        //make sure only can call once at the start
        if (currState !=null || nextState != null)
            return;

        currState = stateMap.get(_newCurrent);
        if (currState != null)
        {
            currState.OnEnter(view);
            nextState = currState;
        }
    }

    public void Exit()
    {
       currState = null;
       nextState = null;
    }
}
