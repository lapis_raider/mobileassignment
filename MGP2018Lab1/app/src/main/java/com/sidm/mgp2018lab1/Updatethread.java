package com.sidm.mgp2018lab1;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.view.SurfaceHolder;

import com.sidm.mgp2018lab1.EntityObjects.AI;

// Customised thread
public class Updatethread extends Thread
{

    static final long targetFPS = 60;

    private Gameview view = null;

    private SurfaceHolder holder = null;

    private boolean isRunning = false;

    public Updatethread(Gameview _view)
    {
        view = _view;
        isRunning = true;
        holder = view.getHolder();

        //Render
        ResourceManager.Instance.Init(view); //can put audio and everything here, for resourcemanager to handle
        AudioManager.Instance.Init(view);
        StateManager.Instance.Init(view);
        GameSystem.Instance.Init(view);
    }

    // Check if things are running
    public boolean IsRunning()
    {
        return isRunning;
    }

    // Check if things are initialized
    public void Initalised()
    {
        isRunning = true;
    }

    // Check if things have ended
    public void Terminate()
    {
        isRunning = false;
    }

    @Override
    public void run()
    {
        // IMPORTANT!!

        // Initialise some variables
        long framePerSecond = 1000 / targetFPS;
        long startTime = 0; // To deal with Frame Rate Controller

        long prevTime = System.nanoTime(); // To deal with Delta Time

        StateManager.Instance.Start("MainGame");
        while (IsRunning())
        {
            startTime = System.currentTimeMillis();

            // Get Delta Time
            long currTime = System.nanoTime();
            float deltaTime = (float)((currTime - prevTime) / 1000000000.0);

            prevTime = currTime; // End of delta time

            // Do some updates
            //Samplegame.Instance.Update(deltaTime);
            StateManager.Instance.Update(deltaTime);

            // Render things
            Canvas canvas = holder.lockCanvas();

            // Check if Canvas is empty or not
            if (canvas != null)
            {
                synchronized (holder) // Canvas is locked
                {
                    // Here you can render things
                    canvas.drawColor(Color.BLUE); //background color

                    // Your sample game scene
                   // Samplegame.Instance.Render(canvas);
                    StateManager.Instance.Render(canvas);
                }
                // Done rendering, can unlock canvas
                holder.unlockCanvasAndPost(canvas);

                if (Player.Instance.changeScene) {
                   EntityManager.Instance.GetEntityList().clear();
                   isRunning = false;
                   Player.Instance.Exit();
                   AI.Instance.Exit();
                   StateManager.Instance.Exit();
                   Gamepage.Instance.ChangeState();
                }
            }

            // Post update
            try
            {
                long sleepTime = framePerSecond - (System.currentTimeMillis() - startTime);

                if (sleepTime > 0)
                {
                    sleep(sleepTime);
                }
            }
            catch (InterruptedException e)
            {
                Terminate();
            }
        }
    }
}
