package com.sidm.mgp2018lab1;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.facebook.login.widget.ProfilePictureView;
import com.facebook.share.Share;
import com.facebook.share.ShareApi;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.ShareButton;
import com.facebook.share.widget.ShareDialog;

import java.util.Arrays;
import java.util.List;

public class Profilepage extends Activity implements OnClickListener
{
    private ImageButton btn_back;

    //for facebook login
    private CallbackManager callbackManager;
    private LoginManager loginManager;
    private static final String EMAIL = "email";

    private LoginButton btn_fbLogin;
    ProfilePictureView profile_pic;
    List<String> PERMISSIONS = Arrays.asList("publish_actions");

    private ProfileTracker profileTracker;

    ShareButton shareButton;
    ShareDialog shareDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        // To make fullscreen
        requestWindowFeature(Window.FEATURE_NO_TITLE); // Hide title
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN); // Hide top bar;
        FacebookSdk.sdkInitialize(this.getApplicationContext()); //init facebook
        setContentView(R.layout.profilepage);

        int score[] = {-1,-1,-1,-1,-1};
        //change the
        //GameSystem.Instance.SaveEditBegin();
        for (int i =0; i <Player.Instance.HIGHSCORE_KEEP; ++i )
        {
            String scoreName = "SAVE" + i;
            if (GameSystem.Instance.GetIntFromSave(scoreName) != 0)
                score[i] = GameSystem.Instance.GetIntFromSave(scoreName);
        }
        //GameSystem.Instance.SaveEditEnd();
        for (int i = 1; i < Player.Instance.HIGHSCORE_KEEP; ++i) //sort the highscore
        {
            Boolean leave = false;
            for (int r =0; r < Player.Instance.HIGHSCORE_KEEP - i; ++r)
            {
                if (score[r + 1] == -1)
                    continue;

                if (score[r] > score[r + 1])
                {
                    //swap
                    int temp = score[r + 1];
                    score[r+ 1] = score[r];
                    score[r] = temp;
                    leave = true;
                }
            }

            if (!leave) //everything is sorted already
                break;
        }
        //set the score
        String highscore = "spent this amount: $";
        String noPlayer = "No one has taken this spot yet, be the next thrifty mom!";

        Button scoreButton = (Button)findViewById(R.id.scoreButton0);
        if (score[0] != -1)
            scoreButton.setText("1st place only "+ highscore + Integer.toString(score[0]));
        else
            scoreButton.setText(noPlayer);

        scoreButton = (Button)findViewById(R.id.scoreButton1);
        if (score[1] != -1)
             scoreButton.setText("2nd place only " + highscore + Integer.toString(score[1]));
        else
            scoreButton.setText(noPlayer);

        scoreButton = (Button)findViewById(R.id.scoreButton2);
        if (score[2] != -1)
            scoreButton.setText("3rd place only "+ highscore + Integer.toString(score[2]));
        else
            scoreButton.setText(noPlayer);

        scoreButton = (Button)findViewById(R.id.scoreButton3);
        if (score[3] != -1)
            scoreButton.setText("4th place only "+highscore + Integer.toString(score[3]));
        else
            scoreButton.setText(noPlayer);

        scoreButton = (Button)findViewById(R.id.scoreButton4);
        if (score[4] != -1 )
            scoreButton.setText("5th place only "+ highscore + Integer.toString(score[4]));
        else
            scoreButton.setText(noPlayer);


        // Set listener to button
        btn_back = (ImageButton)findViewById(R.id.profile_backbtn);
        btn_back.setOnClickListener(this);

        callbackManager = CallbackManager.Factory.create();
        btn_fbLogin = (LoginButton)findViewById(R.id.fb_login_button);
        btn_fbLogin.setReadPermissions(Arrays.asList(EMAIL));
        profile_pic = (ProfilePictureView)findViewById(R.id.picture);

        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "email"));
        AccessTokenTracker accessTokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken oldAccessToken, AccessToken currentAccessToken) {
                if (currentAccessToken == null) //user logged out
                    profile_pic.setProfileId("");
                else {
                    if (Profile.getCurrentProfile() != null) {
                        profile_pic.setProfileId(Profile.getCurrentProfile().getId());
                    }
                }
            }
        };
        accessTokenTracker.startTracking();

        loginManager = LoginManager.getInstance();
        loginManager.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {

                AccessToken accessToken = AccessToken.getCurrentAccessToken();
                loginResult.getAccessToken().getUserId();
                if (Profile.getCurrentProfile() !=null) {
                    profile_pic.setProfileId(Profile.getCurrentProfile().getId());
                }
                else
                {
                    profileTracker = new ProfileTracker() {
                        @Override
                        protected void onCurrentProfileChanged(Profile oldProfile, Profile currentProfile) {
                                profileTracker.stopTracking();
                                Profile.setCurrentProfile(currentProfile);
                                if (Profile.getCurrentProfile() !=null) {
                                    profile_pic.setProfileId(Profile.getCurrentProfile().getId());
                                }
                        }
                    };
                }
            }

            @Override
            public void onCancel() {
            }

            @Override
            public void onError(FacebookException error) {

            }
        });

        Bitmap image = BitmapFactory.decodeResource(getResources(), R.drawable.title);

        SharePhoto photo = new SharePhoto.Builder().setBitmap(image).build();
        SharePhotoContent photoContent = new SharePhotoContent.Builder().addPhoto(photo).build();

        shareButton = (ShareButton)findViewById(R.id.fb_share_button);
        shareDialog = new ShareDialog(this);
        shareButton.setShareContent(photoContent);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode,data);
        if (callbackManager.onActivityResult(requestCode, resultCode,data)) {
            return;
        }
    }

    @Override
    protected void onPause()
    {
        super.onPause();
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
    }

    @Override
    protected void onStop()
    {
        super.onStop();
    }

    @Override // Will happen if there is an on click (touch of button) on this screen or view
    public void onClick(View v)
    {
        Intent intent = new Intent(); // Intent = action to be performed

        // Intent is an object that provides runtime binding eg. 2 or more activities happening at one time
        if (v == btn_back)
        {
            intent.setClass(this, Mainmenu.class);
        }
        startActivity(intent);
    }
}
