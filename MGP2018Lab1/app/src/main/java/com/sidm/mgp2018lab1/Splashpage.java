package com.sidm.mgp2018lab1;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.Window;
import android.view.WindowManager;

import static com.sidm.mgp2018lab1.R.layout.splashpage;

public class Splashpage extends Activity {

    protected boolean _active = true;
    protected int _splashTime = 5000; // Time to display the splash screen

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        // To make fullscreen
        requestWindowFeature(Window.FEATURE_NO_TITLE); // Hide title
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN); // Hide top bar;
        setContentView(R.layout.splashpage);

        GameSystem.Instance.sharedPref =getSharedPreferences("GameSaveFile", Context.MODE_PRIVATE);
        //thread for displaying the Splash Screen
    Thread splashTread = new Thread() {
        @Override
        public void run() {
            try {
                int waited = 0;
                while(_active && (waited < _splashTime)) {
                    sleep(200);
                    if(_active) {
                        waited += 200;
                    }
                }
            } catch(InterruptedException e) {
                //do nothing
            } finally {
                finish();
                //Create new activity based on and intent with CurrentActivity
                Intent intent = new Intent(Splashpage.this, Mainmenu.class);
                startActivity(intent);
            }
        }
    };
 splashTread.start();
}
    @Override
    public boolean onTouchEvent(MotionEvent event){
        if(event.getAction() == MotionEvent.ACTION_DOWN){
            _active = false;
        }
        return true;
    }
}
