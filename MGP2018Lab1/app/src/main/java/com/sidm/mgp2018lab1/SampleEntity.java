package com.sidm.mgp2018lab1;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.SurfaceView;
import android.os.Build;
import android.widget.ImageView;

import java.util.Random;

import com.sidm.mgp2018lab1.grid.GameGrid;
import com.sidm.mgp2018lab1.grid.GridPt;
import com.sidm.mgp2018lab1.utility.Vector2;

public class SampleEntity extends EntityBase   //collidable will be used to get position, x, y, to check whether collided or not
{
    private int renderLayer =0;

    private Vibrator _vibrator;

    @Override
    public void Init(SurfaceView _view)
    {
        //set positions
        //animation = new Sprite(BitmapFactory.decodeResource(_view.getResources(), R.drawable.smurf_sprite),4,4,12 ); //row and col or sprite sheet
        animation = new Sprite(ResourceManager.Instance.GetBitmap(R.drawable.smurf_sprite),4,4,12 ); //new way to get the bitmap

        _vibrator = (Vibrator) _view.getContext().getSystemService(_view.getContext().VIBRATOR_SERVICE);
    }

    @Override
    public void Init(SurfaceView _view, GridPt grid, GameGrid gridData)
    {
        //set positions
        //animation = new Sprite(BitmapFactory.decodeResource(_view.getResources(), R.drawable.smurf_sprite),4,4,12 ); //row and col or sprite sheet
        animation = new Sprite(BitmapFactory.decodeResource(_view.getResources(), R.drawable.walkanimationleft),1,3, 1 );
        gridPos = grid;
        position = new Vector2(grid.x * gridData.grid.m_gridSizeX, grid.y * gridData.grid.m_gridSizeX);

        _vibrator = (Vibrator) _view.getContext().getSystemService(_view.getContext().VIBRATOR_SERVICE);
    }

    public void startVibrate()
    {
        if (Build.VERSION.SDK_INT >= 26)
        {
            _vibrator.vibrate(VibrationEffect.createOneShot(150,10));
        }
        else
        {
            long pattern[] = {0,50,0};
            _vibrator.vibrate(pattern, -1);
        }
    }


    @Override
    public void Update(float _dt) 
	{
        // Update based on dt
        animation.Update(_dt);

        if (TouchManager.Instance.IsDown())
        {
            //check collision here!!
            float imgRadius = animation.GetHeight() * 0.5f;
            //SphereToSphere(float x1, float y1, float radius1, float x2, float y2, float radius2)
            if (Collision.SphereToSphere(TouchManager.Instance.GetPosX(),TouchManager.Instance.GetPosY(),0.0f, position.x, position.y, imgRadius))
            {
                SetIsDone(true);
                startVibrate();
                AudioManager.Instance.PlayAudio(R.raw.correct, 2); //to play audio
            }
        }

    }

    @Override
    public void Render(Canvas _canvas)
    {
        // Render anything
        animation.Render(_canvas, (int)position.x, (int)position.y);
    }

    public static SampleEntity Create(GridPt grid, GameGrid gridData) //add entity here
    {
       SampleEntity result = new SampleEntity();
        EntityManager.Instance.AddEntity(result, grid, gridData);
        return result;
    }

    public static SampleEntity Create() //add entity here
    {
        SampleEntity result = new SampleEntity();
        EntityManager.Instance.AddEntity(result);
        return result;
    }

    public static SampleEntity Create(int _layer) //add entity here
    {
        SampleEntity result = Create();
        result.SetRenderLayer(_layer);
        return result;
    }


    @Override
    public void OnHit(Collidable _other)
    {
        if (_other.GetType() == EntityTypes.DEFAULT) //check for any collision detected, and set the parameter to true
        {
            SetIsDone(true);
        }
    }
}

