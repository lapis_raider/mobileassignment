package com.sidm.mgp2018lab1;

import com.sidm.mgp2018lab1.utility.Vector2;

public interface Collidable {

    public enum EntityTypes
    {
        DEFAULT,
        RUBBISH,
        TELEVISION,
        COMPUTER,
        TAP,
        FRIDGE,
        MOVING_ENTITIES,
    };

    public enum BillType
    {
        NONE,
        WATER,
        ELECTRIC,
    }


    EntityTypes GetType();
    Vector2 GetPos();
    float GetRadius();

    float GetWidth();
    float GetHeight();
    Vector2 GetPosMin();
    Vector2 GetPosMax();

    void OnHit(Collidable _Other);
}
