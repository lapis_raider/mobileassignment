package com.sidm.mgp2018lab1;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.Window;
import android.view.WindowManager;

import com.sidm.mgp2018lab1.Popup.PauseConfirmDialogueStatement;

public class Gamepage extends Activity {

    public static Gamepage Instance = null;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        // To make fullscreen
        requestWindowFeature(Window.FEATURE_NO_TITLE); // Hide title
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN); // Hide top bar;

        Instance = this;
        setContentView(new Gameview(this));
    }

    @Override
    public boolean onTouchEvent(MotionEvent event)
    {
        // WE are hijacking the touch event into our own system
        int x = (int) event.getX();
        int y = (int) event.getY();

        TouchManager.Instance.Update(x, y, event.getAction());

        return true;
    }

    public void ChangeState()
    {
         Intent intent = new Intent().setClass(this, Mainmenu.class);
         this.startActivity(intent);
         Player.Instance.changeScene = false;
    }
}
