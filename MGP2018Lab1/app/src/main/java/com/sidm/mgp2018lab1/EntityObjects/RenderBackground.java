package com.sidm.mgp2018lab1.EntityObjects;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.util.DisplayMetrics;
import android.view.SurfaceView;

import com.sidm.mgp2018lab1.EntityBase;
import com.sidm.mgp2018lab1.EntityManager;
import com.sidm.mgp2018lab1.LayerConstants;
import com.sidm.mgp2018lab1.R;
import com.sidm.mgp2018lab1.Samplegame;

public class RenderBackground extends EntityBase {

    private Bitmap map =null;
    float offset = 0.0f;
    private boolean isDone = false;
    private float xPos, yPos = 0;

    int ScreenWidth, ScreenHeight;
    private Bitmap Scaledmap;


    @Override
    public boolean IsDone()
    {
        return isDone;
    }


    @Override
    public void SetIsDone(boolean _isDone){isDone = _isDone;}


    @Override
    public void Init(SurfaceView _view)
    {
        DisplayMetrics metrics = _view.getResources().getDisplayMetrics();
        ScreenWidth = metrics.widthPixels;
        ScreenHeight = metrics.heightPixels;

        map = BitmapFactory.decodeResource(_view.getResources(), R.drawable.backgroundtemp);
        //map = BitmapFactory.decodeResource(_view.getResources(), R.drawable.grid10x5);
        Scaledmap = Bitmap.createScaledBitmap(map, ScreenWidth, ScreenHeight, true);
    }

    @Override
    public void Update(float dt)
    {
        if (Samplegame.Instance.GetisPaused()) //if pause, then dont do update
            return;

        if (xPos < -ScreenWidth)
            xPos = 0;
    }

    @Override
    public void Render(Canvas _canvas)
    {
        _canvas.drawBitmap(Scaledmap, xPos, yPos, null);
        _canvas.drawBitmap(Scaledmap, xPos + ScreenWidth, yPos, null);
    }

    public boolean IsInit()
    {
        return map != null;
    }

    @Override
    public int GetRenderLayer()
    {
        return LayerConstants.BACKGROUND_LAYER;
    }

    @Override
    public void SetRenderLayer(int _newLayer)
    {
        return;
    }

    public static RenderBackground Create()
    {
        RenderBackground result = new RenderBackground();
        EntityManager.Instance.AddEntity(result);
        return result;
    }
}
