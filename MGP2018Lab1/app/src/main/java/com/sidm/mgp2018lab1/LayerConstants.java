package com.sidm.mgp2018lab1;

//for layering later on, easier for overlaping objects
public class LayerConstants {
    public final static int BACKGROUND_LAYER = 0;
    public final static int RENDERTEXT_LAYER = 1;
    public final static int GAMEOBJECTS_LAYER = 2;
    public final static int MOVING_GAMEOBJECTS_LAYER = 3;
    public final static int UI_LAYER = 100;
    public final static int UI_LAYER2 = 101;
}
