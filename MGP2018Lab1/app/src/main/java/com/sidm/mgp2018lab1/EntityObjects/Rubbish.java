package com.sidm.mgp2018lab1.EntityObjects;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.util.DisplayMetrics;
import android.view.SurfaceView;

import com.sidm.mgp2018lab1.EntityBase;
import com.sidm.mgp2018lab1.EntityManager;
import com.sidm.mgp2018lab1.LayerConstants;
import com.sidm.mgp2018lab1.R;
import com.sidm.mgp2018lab1.SampleEntity;
import com.sidm.mgp2018lab1.grid.GameGrid;
import com.sidm.mgp2018lab1.grid.GridPt;
import com.sidm.mgp2018lab1.utility.Vector2;

import java.util.Random;

public class Rubbish extends EntityBase {

    public Rubbish()
    {
    }

    public Rubbish(GridPt grid, GameGrid gridData)
    {
        gridPos = grid;
        position = new Vector2(gridPos.x * gridData.grid.m_gridSizeX + gridData.grid.m_gridOffsetX , ( gridData.grid.m_sizeY - 1 - gridPos.y) * gridData.grid.m_gridSizeY + gridData.grid.m_gridOffsetY);
    }

    @Override
    public void Init(SurfaceView _view) {
        //set positions
        Random rand = new Random();
        int randomise = rand.nextInt(3); // 0- 2

        if (randomise == 0) {
            spritePic = BitmapFactory.decodeResource(_view.getResources(), R.drawable.rubbishone);
        }
        else if (randomise == 1) {
            spritePic = BitmapFactory.decodeResource(_view.getResources(), R.drawable.rubbishtwo);
        }
        else if (randomise == 2) {
            spritePic = BitmapFactory.decodeResource(_view.getResources(), R.drawable.rubbishthree);
        }

        Scale(0.3f,0.3f); //change Size
        entityTypes = EntityTypes.RUBBISH;
        spritePic = Bitmap.createScaledBitmap(spritePic, (int)sizeWidth, (int)sizeHeight, true);


        DisplayMetrics metrics = _view.getResources().getDisplayMetrics();
        ScreenWidth = metrics.widthPixels;
        ScreenHeight = metrics.heightPixels;

        hasCollider = true;
    }


    @Override
    public void Init(SurfaceView _view, GridPt grid, GameGrid gridData)
    {
        //set positions
        Random rand = new Random();
        int randomise = rand.nextInt(2); // 0- 2

        if (randomise == 0) {
            spritePic = BitmapFactory.decodeResource(_view.getResources(), R.drawable.rubbishone);
        }
        else if (randomise == 1) {
            spritePic = BitmapFactory.decodeResource(_view.getResources(), R.drawable.rubbishtwo);
        }
        else if (randomise == 2) {
            spritePic = BitmapFactory.decodeResource(_view.getResources(), R.drawable.rubbishthree);
        }

        gridPos = grid;
        position = new Vector2(grid.x * gridData.grid.m_gridSizeX + gridData.grid.m_gridOffsetX, grid.y * gridData.grid.m_gridSizeY + gridData.grid.m_gridOffsetY);
        Scale(0.3f,0.3f); //change Size
        entityTypes = EntityTypes.RUBBISH;
        spritePic = Bitmap.createScaledBitmap(spritePic, (int)sizeWidth, (int)sizeHeight, true);


        DisplayMetrics metrics = _view.getResources().getDisplayMetrics();
        ScreenWidth = metrics.widthPixels;
        ScreenHeight = metrics.heightPixels;

        hasCollider = true;
    }


    @Override
    public void Update(float _dt)
    {
        return;
    }

    public static Rubbish Create(GridPt grid, GameGrid gridData) //add entity here
    {
        Rubbish result = new Rubbish();
        EntityManager.Instance.AddEntity(result, grid, gridData);
        return result;
    }

    @Override
    public void Render(Canvas _canvas)
    {
        // Render anything
        _canvas.drawBitmap(spritePic, null, new Rect((int)(position.x - sizeWidth * 0.5f), (int)(position.y - sizeHeight * 0.5f), (int)(position.x + sizeWidth * 0.5f), (int)(position.y + sizeHeight * 0.5f)), null);
    }

    @Override
    public int GetRenderLayer()
    {
        return LayerConstants.GAMEOBJECTS_LAYER;
    }

    @Override
    public void SetRenderLayer(int _newLayout) { return; }

}
