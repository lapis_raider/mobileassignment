package com.sidm.mgp2018lab1;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.util.DisplayMetrics;
import android.view.SurfaceView;

import com.sidm.mgp2018lab1.EntityObjects.UI.ScoreText;
import com.sidm.mgp2018lab1.EntityObjects.UI.DownButton;
import com.sidm.mgp2018lab1.EntityObjects.UI.ElectricIcon;
import com.sidm.mgp2018lab1.EntityObjects.UI.InteractButton;
import com.sidm.mgp2018lab1.EntityObjects.UI.LeftButton;
import com.sidm.mgp2018lab1.EntityObjects.UI.MenuButton;
import com.sidm.mgp2018lab1.EntityObjects.UI.PauseButton;
import com.sidm.mgp2018lab1.EntityObjects.RenderBackground;
import com.sidm.mgp2018lab1.EntityObjects.UI.RightButton;
import com.sidm.mgp2018lab1.EntityObjects.Rubbish;
import com.sidm.mgp2018lab1.EntityObjects.Television;
import com.sidm.mgp2018lab1.EntityObjects.UI.UIPanel;
import com.sidm.mgp2018lab1.EntityObjects.UI.UpButton;
import com.sidm.mgp2018lab1.EntityObjects.UI.WaterIcon;
import com.sidm.mgp2018lab1.grid.GameGrid;
import com.sidm.mgp2018lab1.grid.Grid;
import com.sidm.mgp2018lab1.grid.GridPt;

import java.util.Random;

public class Samplegame {

    // Create a singleton
    public final static Samplegame Instance = new Samplegame();

    // Game stuff...
    float offset = 0.0f;
    float xPos, yPos;
    int ScreenWidth, ScreenHeight;


    // Game map
    private GameGrid gameGrid;
    private Bitmap map;
    private Bitmap Scaledmap;

    private boolean isPaused = false;

    private Samplegame()
    {
    }

    // Init
    // Update
    public float count = 0;
    public void Init(SurfaceView _view)
    {
        DisplayMetrics metrics = _view.getResources().getDisplayMetrics();
        ScreenWidth = metrics.widthPixels;
        ScreenHeight = metrics.heightPixels;

        xPos = yPos = 0;

        map = BitmapFactory.decodeResource(_view.getResources(), R.drawable.backgroundtemp);
        Scaledmap = Bitmap.createScaledBitmap(map, ScreenWidth, ScreenHeight, true);

        EntityManager.Instance.Init(_view);

        // Creation of map object
        Grid.TILE_CONTENT[] array = new Grid.TILE_CONTENT[10 * 5];
        for (int i = 0; i < 10 * 5; ++i)
            array[i] = Grid.TILE_CONTENT.TILE_EMPTY;
        // Set entity pos in Grid here use gameGrid.GridPtIndex(...) to get index to place
        gameGrid = new GameGrid(10, 5, ScreenWidth, ScreenHeight); //create the map
        array[gameGrid.GridPtIndex(new GridPt(9,3))] = Grid.TILE_CONTENT.TILE_OBJECT; // Set TV as obstacle in grid
        gameGrid.SetGrid(array);

        Player.Instance.Init(_view, new GridPt(1, 1), gameGrid);
        count=0;

        //RenderBackground.Create();
        //ScoreText.Create();
    }


    public void Update(float _deltaTime)
    {
        offset += _deltaTime;

        xPos -= _deltaTime * 100;

        if (xPos < -ScreenWidth)
            xPos = 0;

        if (count == 0) //must be called after init once
        {
            // Creating entities
            Random rand = new Random();

            Rubbish.Create(new GridPt(5,5), gameGrid);
            for (int i =0; i < 5; ++i)
            {
                Rubbish.Create(new GridPt(rand.nextInt(gameGrid.grid.m_sizeX - 1) + 1,rand.nextInt(gameGrid.grid.m_sizeY - 1) + 1), gameGrid);
            }
            Television.Create(new GridPt(9, 3), gameGrid);

            InteractButton.Create();
            if (!Player.Instance.optionControl) {
                LeftButton.Create();
                RightButton.Create();
                UpButton.Create();
                DownButton.Create();
            }
            UIPanel.Create();
            ElectricIcon.Create();
            WaterIcon.Create();
            MenuButton.Create();
            PauseButton.Create();
            SampleEntity.Create();
            RenderBackground.Create();
            ScoreText.Create();

            ++count;
        }

        EntityManager.Instance.Update(_deltaTime);
        Player.Instance.Update(_deltaTime);

        if (TouchManager.Instance.IsDown() && !Player.Instance.isMoving && Player.Instance.optionControl && !Player.Instance.interactButton)
        {
            // Setting start and end for BFS
            gameGrid.SetStart(Player.Instance.gridPos.x, Player.Instance.gridPos.y);
            GridPt Index = new GridPt((int)(TouchManager.Instance.GetPosX()/gameGrid.grid.m_gridSizeX), (int)(gameGrid.grid.m_sizeY - 1 - TouchManager.Instance.GetPosY()/gameGrid.grid.m_gridSizeY));
            if (gameGrid.grid.m_grid.get(gameGrid.GridPtIndex(Index)) !=  Grid.TILE_CONTENT.TILE_EMPTY)
            {
                GridPt nearest;
                float shortestDist;
                float tempDist;

                GridPt next = Index.Add(new GridPt(0, 1));
                shortestDist = next.Minus(Player.Instance.gridPos).LengthSquared();
                nearest = next;

                next = Index.Minus(new GridPt(0, 1));
                tempDist = next.Minus(Player.Instance.gridPos).LengthSquared();
                if (shortestDist < tempDist) {
                    shortestDist = tempDist;
                    nearest = next;
                }

                next = Index.Add(new GridPt(1, 0));
                tempDist = next.Minus(Player.Instance.gridPos).LengthSquared();
                if (shortestDist < tempDist) {
                    shortestDist = tempDist;
                    nearest = next;
                }

                next = Index.Minus(new GridPt(1, 0));
                tempDist = next.Minus(Player.Instance.gridPos).LengthSquared();
                if (shortestDist < tempDist) {
                    shortestDist = tempDist;
                    nearest = next;
                }

                gameGrid.SetEnd(next);
            }
            else
                gameGrid.SetEnd(Index);
            gameGrid.BFS(gameGrid.m_start, gameGrid.m_end);

            Player.Instance.SetPath(gameGrid);
        }
    }

    // Render
    public void Render(Canvas _canvas)
    {
        EntityManager.Instance.Render(_canvas); //cause entity manager have their own canvas to render out
        Player.Instance.Render(_canvas);
    }

    //touch on screen, considered as an event


    public boolean GetisPaused() {
        return isPaused;
    }

    public void SetIsPaused(boolean _newPause)
    {
        isPaused = _newPause;
    }
}
