package com.sidm.mgp2018lab1;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.util.DisplayMetrics;
import android.view.SurfaceView;

import com.sidm.mgp2018lab1.EntityObjects.AI;
import com.sidm.mgp2018lab1.EntityObjects.Chair;
import com.sidm.mgp2018lab1.EntityObjects.Computer;
import com.sidm.mgp2018lab1.EntityObjects.FlowerPot;
import com.sidm.mgp2018lab1.EntityObjects.Fridge;
import com.sidm.mgp2018lab1.EntityObjects.RenderBackground;
import com.sidm.mgp2018lab1.EntityObjects.UI.EnemyStunned;
import com.sidm.mgp2018lab1.EntityObjects.UI.FunSprite;
import com.sidm.mgp2018lab1.EntityObjects.UI.LoadingSprite;
import com.sidm.mgp2018lab1.EntityObjects.UI.ScoreText;
import com.sidm.mgp2018lab1.EntityObjects.Table;
import com.sidm.mgp2018lab1.EntityObjects.Tap;
import com.sidm.mgp2018lab1.EntityObjects.Television;
import com.sidm.mgp2018lab1.EntityObjects.UI.DownButton;
import com.sidm.mgp2018lab1.EntityObjects.UI.ElectricIcon;
import com.sidm.mgp2018lab1.EntityObjects.UI.InteractButton;
import com.sidm.mgp2018lab1.EntityObjects.UI.LeftButton;
import com.sidm.mgp2018lab1.EntityObjects.UI.MenuButton;
import com.sidm.mgp2018lab1.EntityObjects.UI.PauseButton;
import com.sidm.mgp2018lab1.EntityObjects.UI.RightButton;
import com.sidm.mgp2018lab1.EntityObjects.UI.ScoreTextPanel;
import com.sidm.mgp2018lab1.EntityObjects.UI.TimePanel;
import com.sidm.mgp2018lab1.EntityObjects.UI.UIPanel;
import com.sidm.mgp2018lab1.EntityObjects.UI.UpButton;
import com.sidm.mgp2018lab1.EntityObjects.UI.WaterIcon;
import com.sidm.mgp2018lab1.grid.GameGrid;
import com.sidm.mgp2018lab1.grid.Grid;
import com.sidm.mgp2018lab1.grid.GridPt;

import java.util.Vector;

public class MainGameState implements StateBase{
    private float timer = 0.0f;

    float offset = 0.0f;
    float xPos, yPos;
    int ScreenWidth, ScreenHeight;

    // Game map
    private GameGrid gameGrid;

    private boolean isPaused = false;

    private Vector<EntityBase> interactable_objs = new Vector<>();

    @Override
    public String GetName() {
        return "MainGame";
    }

    @Override
    public void OnEnter(SurfaceView _view)
    {
        DisplayMetrics metrics = _view.getResources().getDisplayMetrics();
        ScreenWidth = metrics.widthPixels;
        ScreenHeight = metrics.heightPixels;

        xPos = yPos = 0;

        EntityManager.Instance.Init(_view);

        // Creation of map object
        Grid.TILE_CONTENT[] array = new Grid.TILE_CONTENT[10 * 5];
        for (int i = 0; i < 10 * 5; ++i)
            array[i] = Grid.TILE_CONTENT.TILE_EMPTY;
        // Set entity pos in Grid here use gameGrid.GridPtIndex(...) to get index to place
        gameGrid = new GameGrid(10, 5, ScreenWidth, ScreenHeight); //create the map
        array[gameGrid.GridPtIndex(9,3)] = Grid.TILE_CONTENT.TILE_OBJECT; // Set TV as obstacle in grid
        array[gameGrid.GridPtIndex(3,4)] = Grid.TILE_CONTENT.TILE_OBJECT; // Set Tap as obstacle in grid
        array[gameGrid.GridPtIndex(0,2)] = Grid.TILE_CONTENT.TILE_OBJECT; // Set Computer as obstacle in grid
        array[gameGrid.GridPtIndex(6,4)] = Grid.TILE_CONTENT.TILE_OBJECT;

        array[gameGrid.GridPtIndex(5,2)] = Grid.TILE_CONTENT.TILE_OBJECT; // Set FlowerPot as obstacle in grid
        array[gameGrid.GridPtIndex(6,1)] = Grid.TILE_CONTENT.TILE_OBJECT; // Set FlowerPot as obstacle in grid
        array[gameGrid.GridPtIndex(3,1)] = Grid.TILE_CONTENT.TILE_OBJECT; // Set Chair as obstacle in grid
        array[gameGrid.GridPtIndex(3,2)] = Grid.TILE_CONTENT.TILE_OBJECT; // Set Chair as obstacle in grid
        array[gameGrid.GridPtIndex(6,2)] = Grid.TILE_CONTENT.TILE_OBJECT; // Set Chair as obstacle in grid

        gameGrid.SetGrid(array);
        Player.Instance.Create(new GridPt(1, 1), gameGrid);
        AI.Instance.Create(_view, new GridPt(5, 0), gameGrid);
        count=0;
    }

    @Override
    public void OnExit() {
        EntityManager.Instance.Destroy();
    }

    @Override
    public void Render(Canvas _canvas)
    {
        EntityManager.Instance.Render(_canvas); //cause entity manager have their own canvas to render out
    }

    public float count = 0;
    @Override
    public void Update(float _dt) {

        offset += _dt;

        xPos -= _dt * 100;

        if (xPos < -ScreenWidth)
            xPos = 0;

        if (count == 0) //must be called after init once
        {
            AudioManager.Instance.PlayAudio(R.raw.background_music, 0.2f);
            //Random rand = new Random();
            // Creating entities
            interactable_objs.add(Television.Create(new GridPt(9, 3), gameGrid));
            interactable_objs.add(Tap.Create(new GridPt(3, 4), gameGrid));
            interactable_objs.add(Computer.Create(new GridPt(0, 2), gameGrid));
            interactable_objs.add(Fridge.Create(new GridPt(6, 4), gameGrid));
            //Table.Create(new GridPt(5, 1), gameGrid);
            FlowerPot.Create(new GridPt(5, 2), gameGrid);
            FlowerPot.Create(new GridPt(6, 1), gameGrid);
            Chair.Create(new GridPt(3, 1), gameGrid);
            Chair.Create(new GridPt(3, 2), gameGrid);
            Chair.Create(new GridPt(6, 2), gameGrid);


            AI.Instance.SetInteractables(interactable_objs);

            InteractButton.Create();
            if (!Player.Instance.optionControl) {
                LeftButton.Create();
                RightButton.Create();
                UpButton.Create();
                DownButton.Create();
            }
            UIPanel.Create();
            ElectricIcon.Create();
            WaterIcon.Create();
            MenuButton.Create();
            SampleEntity.Create();
            RenderBackground.Create();
            ScoreText.Create();
            ScoreTextPanel.Create();
            LoadingSprite.Create();
            FunSprite.Create();
            EnemyStunned.Create();
            TimePanel.Create();
            ++count;
        }

        EntityManager.Instance.Update(_dt);
        //Player.Instance.Update(_dt);

        if (!Player.Instance.busy) {
            if (TouchManager.Instance.IsDown() && !Player.Instance.isMoving && Player.Instance.optionControl && !Player.Instance.interactButton) {
                // Setting start and end for BFS
                gameGrid.SetStart(Player.Instance.gridPos.x, Player.Instance.gridPos.y);
                GridPt Index = new GridPt((int) (TouchManager.Instance.GetPosX() / gameGrid.grid.m_gridSizeX), (int) (gameGrid.grid.m_sizeY - 1 - TouchManager.Instance.GetPosY() / gameGrid.grid.m_gridSizeY));
                gameGrid.SetEnd(Index);
                gameGrid.BFS(gameGrid.m_start, gameGrid.m_end);
                Player.Instance.SetPath(gameGrid);
            }
        }
    }
}
