package com.sidm.mgp2018lab1.EntityObjects.UI;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.util.DisplayMetrics;
import android.view.SurfaceView;

import com.sidm.mgp2018lab1.EntityBase;
import com.sidm.mgp2018lab1.EntityManager;
import com.sidm.mgp2018lab1.LayerConstants;
import com.sidm.mgp2018lab1.R;
import com.sidm.mgp2018lab1.utility.Vector2;

import java.util.Vector;

public class SpeechBubble extends EntityBase{

    private Bitmap active = null;

    private Boolean isActive;

    private float gridLength = 0;
    private float gridHeight = 0;

    String text;
    char[][] printText;
    int textcol, textrow;
    int textCount;

    @Override
    public void Init(SurfaceView _view, String _text)
    {
        active = BitmapFactory.decodeResource(_view.getResources(), R.drawable.speech_bubble);
        spritePic = active;

        DisplayMetrics metrics = _view.getResources().getDisplayMetrics();
        ScreenWidth = metrics.widthPixels;
        ScreenHeight = metrics.heightPixels;

        //1920 x 1080
        position = new Vector2(1200,300);
        Scale(0.7f,1.f); //change size

        active = Bitmap.createScaledBitmap(active, (int)sizeWidth, (int)sizeHeight, true);
        spritePic = active;

        text = _text;
        printText = new char[4][text.length()];
        textcol = textrow = textCount = 0;

        isActive = false;
    }


    @Override
    public void Update(float _dt)
    {
        if (textCount < text.length())
        {
            if (text.charAt(textCount) == '\n')
            {
                ++textrow;
                textcol = 0;
                ++textCount;
            }
            else
            {
                printText[textrow][textcol] = text.charAt(textCount);
                ++textCount;
                ++textcol;
            }
        }
    }

    public static SpeechBubble Create(String _text) //add entity here
    {
        SpeechBubble result = new SpeechBubble();
        EntityManager.Instance.AddEntity(result, _text);
        return result;
    }

    @Override
    public void Render(Canvas _canvas)
    {
        // Render anything
        _canvas.drawBitmap(spritePic, null, new Rect((int)(position.x - sizeWidth * 0.5f), (int)(position.y - sizeHeight * 0.5f), (int)(position.x + sizeWidth * 0.5f), (int)(position.y + sizeHeight * 0.5f)), null);

        Paint paint = new Paint();
        paint.setARGB(255, 0,0,0);
        paint.setStrokeWidth(200);
        paint.setTextSize(50);

        _canvas.drawText(new String(printText[0]), position.x - 270, position.y - 85, paint);
        _canvas.drawText(new String(printText[1]), position.x - 270, position.y - 35, paint);
        _canvas.drawText(new String(printText[2]), position.x - 270, position.y + 15, paint);
        _canvas.drawText(new String(printText[3]), position.x - 270, position.y + 65, paint);
    }

    @Override
    public Vector2 GetPosMax()
    {
        //  return new Vector2();
        return new Vector2(position.x + sizeWidth/2 + gridLength ,position.y + sizeHeight /2 + gridHeight);
    }

    @Override
    public Vector2 GetPosMin()
    {
        //  return new Vector2();
        return new Vector2(position.x - sizeWidth/2 - gridLength ,position.y - sizeHeight /2 - gridHeight);
    }

    @Override
    public void SetActive(boolean On)
    {
        isActive = On;
    }

    @Override
    public Boolean GetIsActive()
    {
        return isActive;
    }

    @Override
    public int GetRenderLayer()
    {
        return LayerConstants.UI_LAYER;
    }

    @Override
    public void SetRenderLayer(int _newLayout) { return; }

}
