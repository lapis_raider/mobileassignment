package com.sidm.mgp2018lab1.EntityObjects.UI;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.view.SurfaceView;

import com.sidm.mgp2018lab1.EntityBase;
import com.sidm.mgp2018lab1.EntityManager;
import com.sidm.mgp2018lab1.LayerConstants;
import com.sidm.mgp2018lab1.Player;
import com.sidm.mgp2018lab1.StateManager;

public class ScoreText extends EntityBase {

    // Paint object
    Paint paint = new Paint();
    private int red = 0, green = 0, blue = 0;

    private boolean isDone = false;
    private boolean isInit = false;

    int frameCount;
    long lastTime = 0;
    long lastFPSTime = 0;
    float fps;

    Typeface myfont;

    int mins;
    int seconds;

    public boolean IsInit() {
        return isInit;
    }


    @Override
    public boolean IsDone() {
        return isDone;
    }

    @Override
    public void SetIsDone(boolean _isDone) {
        isDone = _isDone;
    }

    @Override
    public void Init(SurfaceView _view) {

        // Week 8 Use my own fonts
        // Load resources for eg. Images
        // To load a background image
        myfont = Typeface.createFromAsset(_view.getContext().getAssets(), "font/Gemcut.otf"); //put in the font file here, name is very impt
    }

    @Override
    public void Update(float _dt) {

        // get actual fps

        frameCount++;

        long currentTime = System.currentTimeMillis();

        lastTime = currentTime;

        if(currentTime - lastFPSTime > 1000)
        {
            fps = (frameCount * 1000.f) / (currentTime - lastFPSTime);
            lastFPSTime = currentTime;
            frameCount = 0;
        }

        mins = (int)(Player.Instance.GetTime() / 60.f);
        seconds = (int)(Player.Instance.GetTime() - (mins * 60.f));
    }

    @Override
    public void Render(Canvas _canvas)
    {
        Paint paint = new Paint();
        paint.setARGB(255, 0,0,0);
        paint.setStrokeWidth(200);
        paint.setTypeface(myfont);
        paint.setTextSize(70);

        if (StateManager.Instance.GetCurrentState() != "LoseState" && StateManager.Instance.GetCurrentState() != "WinState")
        {
            _canvas.drawText("Bills: $" + String.format("%.2f", Player.Instance.elecBill + Player.Instance.waterBill), 85, 100, paint);
            _canvas.drawText("Time: " + Integer.toString(mins) + ":" + Integer.toString( seconds), 1330, 125, paint);
        }
        else
        {
            _canvas.drawText("Bills: $" + String.format("%.2f", Player.Instance.lastBill), 85, 100, paint);
        }
    }

    @Override
    public int GetRenderLayer()
    {
        return LayerConstants.UI_LAYER2;
    }

    @Override
    public void SetRenderLayer(int _newLayer)
    {
        return;
    }

    public static ScoreText Create()
    {
        ScoreText result = new ScoreText();
        EntityManager.Instance.AddEntity(result);
        return result;
    }
}
