package com.sidm.mgp2018lab1.EntityObjects.UI;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.view.SurfaceView;

import com.sidm.mgp2018lab1.EntityBase;
import com.sidm.mgp2018lab1.EntityManager;
import com.sidm.mgp2018lab1.LayerConstants;
import com.sidm.mgp2018lab1.Player;
import com.sidm.mgp2018lab1.R;
import com.sidm.mgp2018lab1.Sprite;
import com.sidm.mgp2018lab1.utility.Vector2;

public class LoadingSprite extends EntityBase{

    private float gridLength = 0;
    private float gridHeight = 0;

    @Override
    public void Init(SurfaceView _view)
    {
        spritePic = BitmapFactory.decodeResource(_view.getResources(), R.drawable.busy);
        position = new Vector2(0,0);
        Scale(0.3f,0.3f); //change size

        animation = new Sprite(Bitmap.createScaledBitmap(spritePic, (int)(sizeWidth), (int)(sizeHeight), true),1,17, 5 );

        IsActive = false;
    }


    @Override
    public void Update(float _dt)
    {
        if (Player.Instance.busy) {
            position = Player.Instance.position.Minus(new Vector2(0, 100));
            animation.Update(_dt);
        }
    }

    public static LoadingSprite Create() //add entity here
    {
        LoadingSprite result = new LoadingSprite();
        EntityManager.Instance.AddEntity(result);
        return result;
    }

    @Override
    public void Render(Canvas _canvas)
    {
        // Render anything
        if (Player.Instance.busy) {
            animation.Render(_canvas, (int) position.x, (int) position.y);
        }
    }

    @Override
    public Vector2 GetPosMax()
    {
        //  return new Vector2();
        return new Vector2(position.x + sizeWidth/2 + gridLength ,position.y + sizeHeight /2 + gridHeight);
    }

    @Override
    public Vector2 GetPosMin()
    {
        //  return new Vector2();
        return new Vector2(position.x - sizeWidth/2 - gridLength ,position.y - sizeHeight /2 - gridHeight);
    }

    @Override
    public void SetActive(boolean On)
    {
        IsActive = On;
    }

    @Override
    public Boolean GetIsActive()
    {
        return IsActive;
    }

    @Override
    public int GetRenderLayer()
    {
        return LayerConstants.MOVING_GAMEOBJECTS_LAYER;
    }

    @Override
    public void SetRenderLayer(int _newLayout) { return; }

}