package com.sidm.mgp2018lab1;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

// Making this class a Surfaceview class
public class Gameview extends SurfaceView
{
    private SurfaceHolder holder = null;

    private Updatethread updatethread = new Updatethread(this);

    public Gameview(Context _context)
    {
        super (_context);
        holder = getHolder();

        if (holder != null)
        {
            holder.addCallback(new SurfaceHolder.Callback() {
                @Override
                public void surfaceCreated(SurfaceHolder holder)
                {
                    // set up stuff to start the gamethread
                    if (!updatethread.IsRunning())
                        updatethread.Initalised();

                    if (!updatethread.isAlive())
                        updatethread.start();

                }

                @Override
                public void surfaceChanged(SurfaceHolder holder, int format, int width, int height)
                {
                    // Do nothing cuz Updatethread will handle it
                }

                @Override
                public void surfaceDestroyed(SurfaceHolder holder)
                {
                    // translate the thread

                    updatethread.Terminate();
                }
            });
        }

    }
}
