package com.sidm.mgp2018lab1;

import android.text.method.Touch;
import android.view.MotionEvent;

//to get touch input on where the coords are and stuff on screen
public class TouchManager {

    public final static TouchManager Instance = new TouchManager();

    private TouchManager(){}

    public enum TouchState
    {
        NONE,
        DOWN,
        MOVE,
    }

    private int posX, posY;
    private TouchState status = TouchState.NONE; //set to default as NONE

    public boolean HasTouch() //Check for a touch status on screen
    {
        return status == TouchState.DOWN || status == TouchState.MOVE;
    }

    public boolean IsDown()
    {
        return status == TouchState.DOWN;
    }

    public int GetPosX()
    {
        return posX;
    }

    public int GetPosY()
    {
        return posY;
    }

    public void Update(int _posX, int _posY, int _motionEventStatus)
    {
        posX = _posX;
        posY = _posY;

        switch (_motionEventStatus) //key movement
        {
            case MotionEvent.ACTION_DOWN: //when key is pressed down
                status = TouchState.DOWN;
                break;

            case MotionEvent.ACTION_MOVE:
                status = TouchState.MOVE;
                break;

            case MotionEvent.ACTION_UP: //when key is up
                status = TouchState.NONE;
                break;
        }
    }
}
