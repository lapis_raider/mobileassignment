package com.sidm.mgp2018lab1;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

public class Mainmenu extends Activity implements OnClickListener
{
    // Define an object & pass it to another method to use
    private Button btn_start;
    private Button btn_options;
    private Button btn_profile;

    // Annotation to assure that the subclass method is overriding the parent class method. If it does not, compile w/ error
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        // To make fullscreen
        requestWindowFeature(Window.FEATURE_NO_TITLE); // Hide title
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN); // Hide top bar;
        setContentView(R.layout.mainmenu);

        // Set listener to button
        btn_start = (Button)findViewById(R.id.btn_start);
        btn_start.setOnClickListener(this);

        btn_options = (Button)findViewById(R.id.btn_options);
        btn_options.setOnClickListener(this);

        btn_profile = (Button)findViewById(R.id.btn_profile);
        btn_profile.setOnClickListener(this);
    }

    @Override // Will happen if there is an on click (touch of button) on this screen or view
    public void onClick(View v)
    {
        Intent intent = new Intent(); // Intent = action to be performed

        // Intent is an object that provides runtime binding eg. 2 or more activities happening at one time
        if (v == btn_start)
        {
           intent.setClass(this, Gamepage.class);
        }
        else if (v == btn_options)
        {
            intent.setClass(this, Optionspage.class);
        }
        else if (v == btn_profile)
        {
            intent.setClass(this, Profilepage.class);
        }
        startActivity(intent);
    }
}
