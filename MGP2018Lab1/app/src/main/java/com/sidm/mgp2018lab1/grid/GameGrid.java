package com.sidm.mgp2018lab1.grid;

import java.util.Vector;

// SceneMaze Equivalent
public class GameGrid {

    // Data declaration
    public Grid grid;
    public GridPt m_start;
    public GridPt m_end;
    public Vector<Boolean> m_visited; // visited set for DFS/BFS
    public Vector<GridPt> m_queue; // queue for BFS;
    public Vector<GridPt> m_previous; // to store previous tile
    public Vector<GridPt> m_shortestPath; // to store shortest path

    public GameGrid(float _sizeX, float _sizeY, float _screenWidth, float _screenHeight)
    {
        grid = new Grid(_sizeX, _sizeY, _screenWidth, _screenHeight);
        m_start = new GridPt();
        m_end = new GridPt();
        m_visited = new Vector<Boolean>();
        m_queue = new Vector<GridPt>();
        m_previous = new Vector<GridPt>();
        m_shortestPath = new Vector<GridPt>();

        m_visited.clear();
        m_queue.clear();
        m_shortestPath.clear();
        m_previous.clear();
        for (int i = 0; i < grid.m_sizeX * grid.m_sizeY; ++i)
            m_visited.add(false);
        for (int i = 0; i < grid.m_sizeX * grid.m_sizeY; ++i)
            m_previous.add(null);
    }

    public void SetGrid( Grid.TILE_CONTENT _grid[] )
    {
        grid.SetGrid(_grid);
    }

    public void SetStart(int x, int y)
    {
        m_start.x = x;
        m_start.y = y;
    }
    public void SetEnd(int x, int y)
    {
        m_end.x = x;
        m_end.y = y;
    }
    public void SetStart(GridPt start)
    {
        m_start.x = start.x;
        m_start.y = start.y;
    }
    public void SetEnd(GridPt end)
    {
        m_end.x = end.x;
        m_end.y = end.y;
    }

    public int GridPtIndex(GridPt pt)
    {
       return pt.x + pt.y * grid.m_sizeX;
    }
    public int GridPtIndex(int x, int y) { return x + y * grid.m_sizeX; }

    // Path Finding BFS
    public boolean BFS(GridPt start, GridPt end)
    {
        m_visited.clear();
        m_queue.clear();
        m_shortestPath.clear();
        m_previous.clear();
        for (int i = 0; i < grid.m_sizeX * grid.m_sizeY; ++i)
            m_visited.add(false);
        for (int i = 0; i < grid.m_sizeX * grid.m_sizeY; ++i)
            m_previous.add(null);
        GridPt nearestTile = start;
        float nearestDistance = Float.MAX_VALUE;

        m_queue.add(start);
        while (m_queue.size() > 0)
        {
            grid.SetCurr(m_queue.firstElement());
            m_queue.remove(0);
            float distanceSqrd = end.Minus(grid.GetCurr()).LengthSquared();
            if (distanceSqrd < nearestDistance)
            {
                nearestDistance = distanceSqrd;
                nearestTile = grid.GetCurr();
                if (grid.GetCurr().isEqual(end))
                {
                    while (!(grid.GetCurr().isEqual(start)))
                    {
                        m_shortestPath.add(0, grid.GetCurr());
                        grid.SetCurr(m_previous.get(GridPtIndex(grid.GetCurr())));
                    }
                    m_shortestPath.add(0, grid.GetCurr());
                    return true;
                }
            }

            // Check UP direction
            GridPt next = new GridPt(grid.GetCurr().x, grid.GetCurr().y + 1);
            int Index = GridPtIndex(next);
            if (next.y < grid.m_sizeY && !m_visited.get(Index) && grid.m_grid.get(Index) == Grid.TILE_CONTENT.TILE_EMPTY)
            {
                m_previous.set(Index, grid.GetCurr());
                m_queue.add(next);
                m_visited.set(Index, true);
            }
            // Check DOWN direction
            GridPt next2 = new GridPt(grid.GetCurr().x, grid.GetCurr().y - 1);
            Index = GridPtIndex(next2);
            if (next2.y >= 0 && !m_visited.get(Index) && grid.m_grid.get(Index) == Grid.TILE_CONTENT.TILE_EMPTY)
            {
                m_previous.set(Index, grid.GetCurr());
                m_queue.add(next2);
                m_visited.set(Index, true);
            }
            // Check LEFT direction
            GridPt next3 = new GridPt(grid.GetCurr().x - 1, grid.GetCurr().y);
            Index = GridPtIndex(next3);
            if (next3.x >= 0 && !m_visited.get(Index) && grid.m_grid.get(Index) == Grid.TILE_CONTENT.TILE_EMPTY)
            {
                m_previous.set(Index, grid.GetCurr());
                m_queue.add(next3);
                m_visited.set(Index, true);
            }
            // Check RIGHT direction
            GridPt next4 = new GridPt(grid.GetCurr().x + 1, grid.GetCurr().y);
            Index = GridPtIndex(next4);
            if (next4.x < grid.m_sizeX && !m_visited.get(Index) && grid.m_grid.get(Index) == Grid.TILE_CONTENT.TILE_EMPTY)
            {
                m_previous.set(Index, grid.GetCurr());
                m_queue.add(next4);
                m_visited.set(Index, true);
            }
        }

        grid.SetCurr(nearestTile);

        while (!grid.GetCurr().isEqual(start))
        {
            m_shortestPath.add(0, grid.GetCurr());
            grid.SetCurr(m_previous.get(GridPtIndex(grid.GetCurr())));
        }
        m_shortestPath.add(0, grid.GetCurr());
        return false;
    }
}
