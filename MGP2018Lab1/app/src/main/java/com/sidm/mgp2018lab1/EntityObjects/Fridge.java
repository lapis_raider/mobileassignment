package com.sidm.mgp2018lab1.EntityObjects;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.util.DisplayMetrics;
import android.view.SurfaceView;

import com.sidm.mgp2018lab1.Collision;
import com.sidm.mgp2018lab1.EntityBase;
import com.sidm.mgp2018lab1.EntityManager;
import com.sidm.mgp2018lab1.LayerConstants;
import com.sidm.mgp2018lab1.Player;
import com.sidm.mgp2018lab1.R;
import com.sidm.mgp2018lab1.SampleEntity;
import com.sidm.mgp2018lab1.TouchManager;
import com.sidm.mgp2018lab1.grid.GameGrid;
import com.sidm.mgp2018lab1.grid.GridPt;
import com.sidm.mgp2018lab1.utility.Vector2;

public class Fridge extends EntityBase {

    private Bitmap fridgeOn = null;
    private Bitmap fridgeOff = null;

    private float gridLength = 0;
    private float gridHeight = 0;


    @Override
    public void Init(SurfaceView _view, GridPt grid, GameGrid gridData)
    {
        fridgeOn = BitmapFactory.decodeResource(_view.getResources(), R.drawable.fridge_on);
        fridgeOff = BitmapFactory.decodeResource(_view.getResources(), R.drawable.fridge_off);
        spritePic = fridgeOff;

        gridPos = grid;
        position = new Vector2(grid.x * gridData.grid.m_gridSizeX + gridData.grid.m_gridOffsetX, (Player.Instance.gameData.grid.m_sizeY - 1 - grid.y) * gridData.grid.m_gridSizeY + gridData.grid.m_gridOffsetY);
        Scale(2.9f,2.9f); //change Size
        entityTypes = EntityTypes.FRIDGE;

        fridgeOn = Bitmap.createScaledBitmap(fridgeOn, (int)sizeWidth, (int)sizeHeight, true);
        fridgeOff = Bitmap.createScaledBitmap(fridgeOff, (int)sizeWidth, (int)sizeHeight, true);
        spritePic = fridgeOff;

        gridHeight = gridData.grid.m_sizeY;
        gridLength= gridData.grid.m_sizeX;

        DisplayMetrics metrics = _view.getResources().getDisplayMetrics();
        ScreenWidth = metrics.widthPixels;
        ScreenHeight = metrics.heightPixels;

        hasCollider = true;
        billType = BillType.ELECTRIC;
        IsActive = false;
    }

    @Override
    public void Update(float _dt)
    {
        if (IsActive && !beingUsed) {
            Player.Instance.elecBill += _dt * 50/100;
        }
    }

    public static Fridge Create(GridPt grid, GameGrid gridData) //add entity here
    {
        Fridge result = new Fridge();
        EntityManager.Instance.AddEntity(result, grid, gridData);
        return result;
    }

    @Override
    public void Render(Canvas _canvas)
    {
        // Render anything
        _canvas.drawBitmap(spritePic, null, new Rect((int)(position.x - sizeWidth * 0.5f), (int)(position.y - sizeHeight * 0.5f), (int)(position.x + sizeWidth * 0.5f), (int)(position.y + sizeHeight * 0.5f)), null);
    }

    @Override
    public Vector2 GetPosMax()
    {
        //  return new Vector2();
        return new Vector2(position.x + sizeWidth/2 + gridLength ,position.y + sizeHeight /2 + gridHeight);
    }

    @Override
    public Vector2 GetPosMin()
    {
        //  return new Vector2();
        return new Vector2(position.x - sizeWidth/2 - gridLength ,position.y - sizeHeight /2 - gridHeight);
    }

    @Override
    public void SetActive(boolean On)
    {
        IsActive = On;

        if (IsActive)
            spritePic = fridgeOn;
        else
            spritePic = fridgeOff;
    }

    @Override
    public Boolean GetIsActive()
    {
        return IsActive;
    }

    @Override
    public int GetRenderLayer()
    {
        return LayerConstants.GAMEOBJECTS_LAYER;
    }

    @Override
    public void SetRenderLayer(int _newLayout) { return; }

}
