package com.sidm.mgp2018lab1.EntityObjects.UI;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.util.DisplayMetrics;
import android.view.SurfaceView;

import com.sidm.mgp2018lab1.EntityBase;
import com.sidm.mgp2018lab1.EntityManager;
import com.sidm.mgp2018lab1.LayerConstants;
import com.sidm.mgp2018lab1.Player;
import com.sidm.mgp2018lab1.R;
import com.sidm.mgp2018lab1.utility.Vector2;

public class CreateEmoji extends EntityBase{

    private Bitmap bmp = null;

    private Boolean isActive;

    private float gridLength = 0;
    private float gridHeight = 0;

    @Override
    public void Init(SurfaceView _view, String type)
    {
        if (type == "sad")
            bmp = BitmapFactory.decodeResource(_view.getResources(), R.drawable.sad);
        else if (type == "happy")
            bmp = BitmapFactory.decodeResource(_view.getResources(), R.drawable.smiley);
        spritePic = bmp;

        DisplayMetrics metrics = _view.getResources().getDisplayMetrics();
        ScreenWidth = metrics.widthPixels;
        ScreenHeight = metrics.heightPixels;

        //1080 x 1920
        position = new Vector2(1200,550);
        Scale(1.5f,1.5f); //change size

        bmp = Bitmap.createScaledBitmap(bmp, (int)sizeWidth, (int)sizeHeight, true);
        spritePic = bmp;

        isActive = false;
    }


    @Override
    public void Update(float _dt)
    {
        if (Player.Instance.electricity)
            isActive = true;
        else
            isActive = false;

        SetActive(isActive);
    }

    public static CreateEmoji Create(String type) //add entity here
    {
        CreateEmoji result = new CreateEmoji();
        EntityManager.Instance.AddEntity(result, type);
        return result;
    }

    @Override
    public void Render(Canvas _canvas)
    {
        // Render anything
        _canvas.drawBitmap(spritePic, null, new Rect((int)(position.x - sizeWidth * 0.5f), (int)(position.y - sizeHeight * 0.5f), (int)(position.x + sizeWidth * 0.5f), (int)(position.y + sizeHeight * 0.5f)), null);
    }

    @Override
    public Vector2 GetPosMax()
    {
        //  return new Vector2();
        return new Vector2(position.x + sizeWidth/2 + gridLength ,position.y + sizeHeight /2 + gridHeight);
    }

    @Override
    public Vector2 GetPosMin()
    {
        //  return new Vector2();
        return new Vector2(position.x - sizeWidth/2 - gridLength ,position.y - sizeHeight /2 - gridHeight);
    }

    @Override
    public void SetActive(boolean On)
    {
        isActive = On;
    }

    @Override
    public Boolean GetIsActive()
    {
        return isActive;
    }

    @Override
    public int GetRenderLayer()
    {
        return LayerConstants.UI_LAYER2;
    }

    @Override
    public void SetRenderLayer(int _newLayout) { return; }

}