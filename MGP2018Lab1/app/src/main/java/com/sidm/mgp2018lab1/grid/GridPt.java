package com.sidm.mgp2018lab1.grid;

public class GridPt {

    public int x,y;

    // Constructors
    public GridPt() { x = 0; y = 0;}
    public GridPt(int _x, int _y) { x = _x; y = _y; }
    public GridPt(GridPt RHS) { x = RHS.x; y = RHS.y; }

    public void Set(int _x, int _y) { x = _x; y = _y; }


    // Operators
    public GridPt Add(GridPt RHS) { return new GridPt(x + RHS.x, y + RHS.y); }
    public GridPt Minus(GridPt RHS) { return new GridPt(x - RHS.x, y - RHS.y); }
    public GridPt Multiply(GridPt RHS) { return new GridPt(x * RHS.x, y * RHS.y); }
    public float Dot(GridPt RHS) { return x * RHS.x + y * RHS.y; }
    public float Length() { return (float)Math.sqrt( x * x + y * y ); }
    public float LengthSquared() { return x * x + y * y; }
    public Boolean isEqual(GridPt other)
    {
        return (x == other.x && y == other.y);
    }
}
