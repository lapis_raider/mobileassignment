package com.sidm.mgp2018lab1.EntityObjects.UI;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.util.DisplayMetrics;
import android.view.SurfaceView;

import com.sidm.mgp2018lab1.Collidable;
import com.sidm.mgp2018lab1.Collision;
import com.sidm.mgp2018lab1.EntityBase;
import com.sidm.mgp2018lab1.EntityManager;
import com.sidm.mgp2018lab1.LayerConstants;
import com.sidm.mgp2018lab1.Player;
import com.sidm.mgp2018lab1.R;
import com.sidm.mgp2018lab1.TouchManager;
import com.sidm.mgp2018lab1.grid.GameGrid;
import com.sidm.mgp2018lab1.grid.GridPt;
import com.sidm.mgp2018lab1.utility.Vector2;

public class InteractButton extends EntityBase {

    private Bitmap buttonNotActive = null;
    private Bitmap buttonInActive = null;

    @Override
    public void Init(SurfaceView _view)
    {
        //set positions
        buttonInActive = BitmapFactory.decodeResource(_view.getResources(), R.drawable.buttoninteractactive);
        buttonNotActive = BitmapFactory.decodeResource(_view.getResources(), R.drawable.buttoninteract);
        spritePic = buttonNotActive; //set as inactive first

        DisplayMetrics metrics = _view.getResources().getDisplayMetrics();
        ScreenWidth = metrics.widthPixels;
        ScreenHeight = metrics.heightPixels;

        //1080 x 1920
        position = new Vector2(1700,950);
        Scale(0.3f,0.3f); //change Size

        buttonInActive = Bitmap.createScaledBitmap(buttonInActive, (int)sizeWidth, (int)sizeHeight, true);
        buttonNotActive = Bitmap.createScaledBitmap(buttonNotActive, (int)sizeWidth, (int)sizeHeight, true);
        spritePic = buttonNotActive;
    }

    @Override
    public void Init(SurfaceView _view, GridPt grid, GameGrid gridData)
    {
        //set positions
        buttonInActive = BitmapFactory.decodeResource(_view.getResources(), R.drawable.buttoninteractactive);
        buttonNotActive = BitmapFactory.decodeResource(_view.getResources(), R.drawable.buttoninteract);
        spritePic = buttonNotActive; //set as inactive first

        gridPos = grid;
        position = new Vector2(grid.x * gridData.grid.m_gridSizeX, grid.y * gridData.grid.m_gridSizeX);

        DisplayMetrics metrics = _view.getResources().getDisplayMetrics();
        ScreenWidth = metrics.widthPixels;
        ScreenHeight = metrics.heightPixels;
    }

    @Override
    public void Update(float _dt)
    {
        // Update based on dt
        //animation.Update(_dt);
        if (!TouchManager.Instance.IsDown()) {
            Player.Instance.interactButton = false;
            spritePic = buttonNotActive;
        }

        if (TouchManager.Instance.IsDown()) //if tap on the interact button
        {
            if (Collision.SphereToSphere(TouchManager.Instance.GetPosX(),TouchManager.Instance.GetPosY(), 0.f, position.x, position.y, GetRadius()))
            {
                spritePic = buttonInActive; //make button change look
                Player.Instance.interactButton = true;
            }
        }
    }

    @Override
    public void Render(Canvas _canvas)
    {
        // Render anything
        _canvas.drawBitmap(spritePic, null, new Rect((int)(position.x - sizeWidth * 0.5f), (int)(position.y - sizeHeight * 0.5f), (int)(position.x + sizeWidth * 0.5f), (int)(position.y + sizeHeight * 0.5f)), null);
    }


    public static InteractButton Create() //add entity here
    {
        InteractButton result = new InteractButton();
        EntityManager.Instance.AddEntity(result);
        return result;
    }

    @Override
    public void OnHit(Collidable _other)
    {
       return;
    }

    @Override
    public int GetRenderLayer()
    {
        return LayerConstants.UI_LAYER;
    }

    @Override
    public void SetRenderLayer(int _newLayout) { return; }
}
