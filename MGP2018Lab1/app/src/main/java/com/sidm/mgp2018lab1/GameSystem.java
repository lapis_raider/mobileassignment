package com.sidm.mgp2018lab1;

import android.content.SharedPreferences;
import android.view.SurfaceView;

public class GameSystem {
    public final static GameSystem Instance = new GameSystem();
    public static final String SHARED_PREF_ID = "GameSaveFile"; //save data

    //shared pref can help us get info, store into memory, like for highscore etc instead of using txt file
    private boolean isPaused = false;
    SharedPreferences sharedPref = null;
    SharedPreferences.Editor editor = null;

    private GameSystem()
    {
    }

    public void Init(SurfaceView _view)
    {
        sharedPref = Gamepage.Instance.getSharedPreferences(SHARED_PREF_ID, 0);

        //we will add all of our state into the state manager here
        //samples, load it in our splash page eg.
        //StateManager.Instance.AddState(new IntroState());
        StateManager.Instance.AddState(new MainGameState());
        StateManager.Instance.AddState(new WinState());
        StateManager.Instance.AddState(new LoseState());
        isPaused = false;
    }

    public void Update(float _deltaTime)
    {
    }

    public void SetIsPaused(boolean _newIsPaused)
    {

        isPaused = _newIsPaused;
    }

    public boolean GetIsPaused()
    {
        return isPaused;
    }

    public void SaveEditBegin()
    {
        if (editor != null)
            return;

        editor = sharedPref.edit();
    }

    public void SaveEditEnd()
    {
        if (editor == null)
            return;

        editor.commit();
        editor = null;
    }

    public void SetIntInSave(String _key, int _value)
    {
        if (editor == null)
            return;

        editor.putInt(_key, _value);
    }

    public int GetIntFromSave(String _key)
    {
        int i = sharedPref.getInt(_key, 0);
        return i;
    }
}
