package com.sidm.mgp2018lab1;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.util.DisplayMetrics;
import android.view.SurfaceView;

import com.sidm.mgp2018lab1.grid.GameGrid;
import com.sidm.mgp2018lab1.grid.GridPt;
import com.sidm.mgp2018lab1.utility.Vector2;

import java.util.Vector;


public abstract class EntityBase implements Collidable
{
   //Init any variables here
   protected int ScreenWidth, ScreenHeight;

   protected Sprite animation = null; //check if the sprite will have animation or not
   protected Bitmap spritePic = null;

   public Vector2 position = new Vector2();
   protected GridPt gridPos = new GridPt();

   protected float sizeHeight = 0;
   protected float sizeWidth = 0;

   protected EntityTypes entityTypes = EntityTypes.DEFAULT;
   protected BillType billType = BillType.NONE;
   protected boolean IsActive = false;
   protected boolean hasCollider = false;

   boolean isDone = false;
   protected boolean beingUsed = false; //if its being used by the AI

   protected boolean IsDone(){return isDone;}
   protected void SetIsDone(boolean _isDone){isDone = _isDone;}

   protected void Init(SurfaceView _view)
   {
      entityTypes = EntityTypes.DEFAULT;
      billType = BillType.NONE;
      IsActive = false;
      hasCollider = false;
      DisplayMetrics metrics = _view.getResources().getDisplayMetrics();
      ScreenWidth = metrics.widthPixels;
      ScreenHeight = metrics.heightPixels;
   }

   protected void Init(SurfaceView _view, GridPt grid, GameGrid gridData)
   {
    //set positions
      gridPos = grid;
      position = new Vector2(grid.x * gridData.grid.m_gridSizeX, grid.y * gridData.grid.m_gridSizeX);

      DisplayMetrics metrics = _view.getResources().getDisplayMetrics();
      ScreenWidth = metrics.widthPixels;
      ScreenHeight = metrics.heightPixels;
   }

   protected void Init(SurfaceView _view, String _text)
   {

   }

   protected void Update(float _dt){};
   protected void Render(Canvas _canvas){};

   protected void Scale (float numberToScaleHeight, float numberToScaleWidth)
   {
      if (spritePic != null) {
         sizeHeight = (spritePic.getHeight() * numberToScaleHeight);
         sizeWidth = (spritePic.getWidth() * numberToScaleWidth);
      }
      else
      {
         sizeHeight = numberToScaleHeight;
         sizeWidth = numberToScaleWidth;
      }
   }


   @Override
   public Vector2 GetPosMin() {
       return new Vector2(position.x - sizeWidth/2,position.y - sizeHeight/2);
   }

   @Override
   public Vector2 GetPosMax()
   {
       return new Vector2(position.x + sizeWidth/2 ,position.y + sizeHeight /2);
   }

   @Override
   public Vector2 GetPos() {
      return position;
   }

   @Override
   public float GetRadius() {

      if (animation != null)
         return animation.GetHeight() * 0.5f;

       return spritePic.getWidth() * 0.5f;
   }

   @Override
   public EntityTypes GetType() {
      return entityTypes;
   }

   @Override
   public float GetWidth() {
      return sizeWidth;
   }

   public float GetHeight() {
      return sizeHeight;
   }


   @Override
   public void OnHit(Collidable _other)
   {
      if (_other.GetType() == EntityTypes.DEFAULT) //check for any collision detected, and set the parameter to true
      {
         SetIsDone(true);
      }
   }

   // for applicance if turned on or not
   public Boolean GetIsActive()
   {
      return false;
   }

   public void SetActive(boolean On)
   {
   }

   public Boolean GetIsBeingUsed()
   {
      return beingUsed;
   }

   public void SetIsBeingUsed(boolean use)
   {
      beingUsed =use;
   }

   public int GetRenderLayer() { return 0;} //just render default for now
   public void SetRenderLayer(int _newLayout) {}

   public GridPt GetGridPos() {
      return gridPos;
   }
}

