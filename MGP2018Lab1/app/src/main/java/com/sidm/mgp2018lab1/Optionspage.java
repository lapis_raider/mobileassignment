package com.sidm.mgp2018lab1;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.TextView;

public class Optionspage extends Activity implements OnClickListener
{
    private static SeekBar _seekbar;
    private TextView _seekBarCurrentText;
    private ImageButton btn_back;
    private CheckBox dPadCtrl;
    private CheckBox tapCtrl;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        // To make fullscreen
        requestWindowFeature(Window.FEATURE_NO_TITLE); // Hide title
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN); // Hide top bar;
        setContentView(R.layout.optionspage);
        seekbar();

        // Set listener to button
        btn_back = (ImageButton)findViewById(R.id.option_backbtn);
        btn_back.setOnClickListener(this);
        addListnerToCheckBox();
    }

    public void seekbar() {
        _seekbar = (SeekBar) findViewById(R.id.seekBar);
        _seekBarCurrentText = (TextView) findViewById(R.id.ScrollNumberFinal);
        _seekbar.setProgress((int)(AudioManager.Instance.GetAudioLevel() * 100.f));
        _seekBarCurrentText.setText(String.valueOf(_seekbar.getProgress()));

        _seekbar.setOnSeekBarChangeListener(
                new SeekBar.OnSeekBarChangeListener() {
                    @Override
                    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                        _seekBarCurrentText.setText(String.valueOf(_seekbar.getProgress()));
                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {
                    }

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {
                        float pro = _seekbar.getProgress()/ 100.0f;
                        AudioManager.Instance.SetAudioLevel(pro);
                    }
                }
        );
    }

    @Override // Will happen if there is an on click (touch of button) on this screen or view
    public void onClick(View v)
    {
        Intent intent = new Intent(); // Intent = action to be performed

        // Intent is an object that provides runtime binding eg. 2 or more activities happening at one time
        if (v == btn_back)
        {
            intent.setClass(this, Mainmenu.class);
        }
        startActivity(intent);
    }

    public void addListnerToCheckBox() {
        dPadCtrl = (CheckBox)findViewById(R.id.checkBox);
        tapCtrl = (CheckBox)findViewById(R.id.checkBox2);

        if (Player.Instance.optionControl) //if its dPadCtrl
        {
            dPadCtrl.setChecked(true);
            tapCtrl.setChecked(false);
        }
        else
        {
            dPadCtrl.setChecked(false);
            tapCtrl.setChecked(true);
        }

        dPadCtrl.setOnClickListener(new View.OnClickListener() {
                                      @Override
                                      public void onClick(View v) {
                                          if (((CheckBox) v).isChecked()) {
                                                tapCtrl.setChecked(false);
                                                Player.Instance.optionControl = true;
                                          }
                                      }
                                  } );

        tapCtrl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((CheckBox) v).isChecked()) {
                    dPadCtrl.setChecked(false);
                    Player.Instance.optionControl = false;
                    } }} );
    }
}
