package com.sidm.mgp2018lab1;

import com.sidm.mgp2018lab1.utility.Vector2;

import java.lang.Math;

public class Collision {

    //can add another one to check AABB
    public static boolean PointToBox(Vector2 coords,Vector2 maxCoordsBox, Vector2 minCoordsBox )
    {
        if (coords.x < maxCoordsBox.x && coords.x > minCoordsBox.x && coords.y < maxCoordsBox.y && coords.y > minCoordsBox.y)
            return true;

        return false;
    }


    public static boolean BoxToBox(Vector2 minCoords, Vector2 maxCoords, Vector2 minCoords2Other,  Vector2 maxCoords2Other)
    {
        if (maxCoords.x > minCoords2Other.x && maxCoords.x < maxCoords2Other.x && minCoords.y < maxCoords2Other.y && minCoords.y > minCoords2Other.y) //bottom right inside other box
            return true;
        else if (maxCoords.x > minCoords2Other.x && maxCoords.x < maxCoords2Other.x && maxCoords.y > minCoords2Other.y && maxCoords.y < minCoords2Other.y) //check top right
            return true;
        else if (minCoords.x > minCoords2Other.x && minCoords.x <maxCoords2Other.x && maxCoords.y > minCoords2Other.y && maxCoords.y <  minCoords2Other.y ) //check top left
            return true;
        else if (minCoords.x >  minCoords2Other.x && minCoords.x < maxCoords2Other.x && minCoords.y < maxCoords2Other.y && minCoords.y > minCoords2Other.y )
            return true;

        return false;
    }

    public static boolean SphereToSphere(float x1, float y1, float radius1, float x2, float y2, float radius2)
    {
        //geting coords diff
        float xVec = x2 - x1;
        float yVec = y2-y1;

        float distSquared = xVec * xVec + yVec * yVec;
        float rSquared = radius1 + radius2;
        rSquared *= rSquared;

        if (distSquared > rSquared) //no collision
            return false;
        return true;
    }

}
