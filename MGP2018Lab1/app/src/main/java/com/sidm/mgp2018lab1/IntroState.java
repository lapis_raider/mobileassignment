package com.sidm.mgp2018lab1;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.view.SurfaceView;

// Sample of an intro state - You can change to Splash page..
public class IntroState implements StateBase{
    private float timer = 5.0f;
    private Bitmap logo = null;
    private Sprite spritesheet = null;

    @Override
    public String GetName() {
        return "Default";
    }

    @Override
    public void OnEnter(SurfaceView _view)
    {
        spritesheet = new Sprite(ResourceManager.Instance.GetBitmap(R.drawable.smurf_sprite), 4, 4, 12);
    }

    @Override
    public void OnExit()
    {
    }

    @Override
    public void Render(Canvas _canvas)
    {
        // Render anything
        spritesheet.Render(_canvas, 200, 500);
    }

    @Override
    public void Update(float _dt)
    {
        spritesheet.Update(_dt);

        timer -= _dt;
        if (timer <= 0.0f)
        {
            // We are done showing our splash screen
            // Move on time!
            StateManager.Instance.ChangeState("MainGame");
        }
    }
}
