package com.sidm.mgp2018lab1.EntityObjects;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.view.SurfaceView;

import com.sidm.mgp2018lab1.Collidable;
import com.sidm.mgp2018lab1.EntityBase;
import com.sidm.mgp2018lab1.EntityManager;
import com.sidm.mgp2018lab1.LayerConstants;
import com.sidm.mgp2018lab1.Player;
import com.sidm.mgp2018lab1.R;
import com.sidm.mgp2018lab1.Sprite;
import com.sidm.mgp2018lab1.grid.GameGrid;
import com.sidm.mgp2018lab1.grid.Grid;
import com.sidm.mgp2018lab1.grid.GridPt;
import com.sidm.mgp2018lab1.utility.Vector2;

import java.util.Random;
import java.util.Vector;

import static com.sidm.mgp2018lab1.Player.MovingDir.NONE;

public class AI extends EntityBase {

    public final static AI Instance = new AI();

    //spritesheets
    Sprite leftWalkingAnim = null;
    Sprite rightWalkingAnim =null;
    Sprite upWalkingAnim = null;
    Sprite downWalkingAnim = null;

    Bitmap leftStaticSprite = null;
    Bitmap rightStaticSprite = null;
    Bitmap downStaticSprite = null;
    Bitmap upStaticSprite = null;


    public enum MovingDir
    {
        NONE,
        MOVERIGHT,
        MOVELEFT,
        MOVEUP,
        MOVEDOWN
    }
    private MovingDir dirMoved = MovingDir.NONE;

    public enum AI_STATES
    {
        USE_INTERACTBLE, //will on the TV, sit on the sofa and watch, when watching electricity not waste
        DAILY_ROUTINE, //go to a render on item, to see
        DEFAULT, //go about turning on things and throwing trash
        WANDER, //wander around throwing trash
        STUNNED, //collided with the player, gets stun for a while
    }
    private AI_STATES ai_states = AI_STATES.DEFAULT;
    private AI_STATES before_stun_state = AI_STATES.DEFAULT;

    //VARIABLES
    public GameGrid gameData;
    GridPt gridTarget;
    Vector2 target;
    Vector<GridPt> m_shortest = new Vector<>();
    Vector<EntityBase> m_interactables = new Vector<>();
    boolean isMoving;
    boolean reachLocation;

    // Store so I dont have to keep reusing GameGrid to pass
    float m_gridSizeX, m_gridSizeY;
    float m_gridOffsetX, m_gridOffsetY;

    int RUBBISH_DROPRATE = 2; //1 out of 5 chances
    int CHANGESTATE_CHANCE = 5;

    @Override
    public void Init(SurfaceView _view, GridPt grid, GameGrid gridData) {

        //init AI data here
        gameData = gridData; //get the gridData
        m_gridSizeX = gridData.grid.m_gridSizeX;
        m_gridSizeY = gridData.grid.m_gridSizeY;
        m_gridOffsetX = gridData.grid.m_gridOffsetX;
        m_gridOffsetY= gridData.grid.m_gridOffsetY;

        gridPos = new GridPt(grid.x, grid.y);
        gridTarget = gridPos;
        position = new Vector2(gridPos.x * m_gridSizeX + m_gridOffsetX , ( gameData.grid.m_sizeY - 1 - gridPos.y) * m_gridSizeY + m_gridOffsetY);
        target = position;

        //initialise sprite and animation
        downStaticSprite = BitmapFactory.decodeResource(_view.getResources(), R.drawable.ai_static_down);
        upStaticSprite = BitmapFactory.decodeResource(_view.getResources(), R.drawable.ai_static_up);
        leftStaticSprite = BitmapFactory.decodeResource(_view.getResources(), R.drawable.ai_static_left);
        rightStaticSprite = BitmapFactory.decodeResource(_view.getResources(), R.drawable.ai_static_right);

        spritePic = downStaticSprite;
        Scale(2.f,2.f); //change Size

        //change sizes here
        downStaticSprite = Bitmap.createScaledBitmap(downStaticSprite, (int)sizeWidth, (int)sizeHeight, true);
        upStaticSprite = Bitmap.createScaledBitmap(upStaticSprite, (int)sizeWidth, (int)sizeHeight, true);
        leftStaticSprite = Bitmap.createScaledBitmap(leftStaticSprite, (int)sizeWidth, (int)sizeHeight, true);
        rightStaticSprite = Bitmap.createScaledBitmap(rightStaticSprite, (int)sizeWidth, (int)sizeHeight, true);

        leftWalkingAnim = new Sprite(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(_view.getResources(), R.drawable.ai_walkingleft), (int)(sizeWidth), (int)(sizeHeight), true),1,3, 1 );
        rightWalkingAnim = new Sprite(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(_view.getResources(), R.drawable.ai_walkingright), (int)(sizeWidth), (int)(sizeHeight), true),1,3, 1 );
        upWalkingAnim = new Sprite(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(_view.getResources(), R.drawable.ai_walkingup), (int)(sizeWidth ), (int)(sizeHeight), true),1,3, 1 );
        downWalkingAnim = new Sprite(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(_view.getResources(), R.drawable.ai_walkingdown), (int)(sizeWidth ), (int)(sizeHeight), true),1,3, 1 );
        animation = downWalkingAnim;

        isMoving = false;
        reachLocation = false;

        entityTypes = EntityTypes.MOVING_ENTITIES;
    }


    public void Exit()
    {
        spritePic = null;

        //change sizes here
        leftStaticSprite = null;
        upStaticSprite = null;
        downStaticSprite = null;
        rightStaticSprite = null;

        leftWalkingAnim = null;
        rightWalkingAnim = null;
        upWalkingAnim = null;
        downWalkingAnim = null;

        animation = null;

        //set positions
        gridPos = null;
        gridTarget = null;
        position = null;
        ai_states = AI_STATES.DEFAULT;

        target = null;
        isMoving = false;
        reachLocation = false;
        m_shortest.clear();
    }

    @Override
    public void Update(float _dt) {

        if (ai_states == AI_STATES.DEFAULT)
        {
            DefaultState(_dt);
        }
        else if (ai_states == AI_STATES.USE_INTERACTBLE) {
            UseInteractable(_dt);
        }
        else if (ai_states == AI_STATES.WANDER) {
            WanderState(_dt);
        }
        else if (ai_states == AI_STATES.DAILY_ROUTINE) {
            DailyRoutine(_dt);
        }
        else if (ai_states == AI_STATES.STUNNED) {
            StunnedRoutine(_dt);
        }
    }

    public static AI Create(SurfaceView _view, GridPt grid, GameGrid gridData) //add entity here
    {
        Instance.Init(_view, grid, gridData);
        EntityManager.Instance.AddEntity(Instance, grid, gridData); //calls for me init already
        return Instance;
    }

    @Override
    public void Render(Canvas _canvas)
    {
        if (isMoving) //if moving do the animation
            animation.Render(_canvas, (int)position.x, (int)position.y);
        else
            _canvas.drawBitmap(spritePic, null, new Rect((int)(position.x - sizeWidth * 0.5f), (int)(position.y - sizeHeight * 0.5f), (int)(position.x + sizeWidth * 0.5f), (int)(position.y + sizeHeight * 0.5f)), null);
    }

    public void WanderState(float _dt)
    {
        if (isMoving)
            Movement(_dt); //if not moving, do the BFS
        else
        {
            if (reachLocation) //reach the new location
            {
                //change states
                Random random = new Random();
                int randomize = random.nextInt(7);
                reachLocation = false;

                if (randomize < 5) {
                    ai_states = AI_STATES.DEFAULT;
                    return;
                }
                else if (randomize < 6) {
                    ai_states = AI_STATES.DAILY_ROUTINE;
                    return;
                }
            }

            Random rand = new Random();
            BFS(new GridPt(rand.nextInt(gameData.grid.m_sizeX - 1), rand.nextInt(gameData.grid.m_sizeY - 1)));
            isMoving = true;
        }
    }

    private int item; //the last item used
    public void DefaultState(float _dt)
    {
        if (isMoving)
            Movement(_dt); //if not moving, do the BFS
        else
        {
            Random rand = new Random();
            if (reachLocation) //once it reaches location, it will decide what to do next
            {
                reachLocation = false;
                m_interactables.get(item).SetActive(true);

                //choose whether to use the item, or do something else
                int randomizeDecision = rand.nextInt(10);

                 if (randomizeDecision < 2) {
                    ai_states = AI_STATES.WANDER;
                  return;
                }
                 else if (randomizeDecision < 4) {
                     ai_states = AI_STATES.DAILY_ROUTINE;
                     return;
                }
                else if (randomizeDecision <= 5) {
                     ai_states = AI_STATES.USE_INTERACTBLE;
                     return;
                 }

            }

            if (CheckEveryItemPower()) //at least one item still have not been turned on
            {
                do {
                    item = rand.nextInt(m_interactables.size()); //get one of the locations from the interactables
                }while(m_interactables.get(item).GetIsActive());

                BFS(m_interactables.get(item).GetGridPos());
                isMoving = true;
            }
            else
            {
                //change AI state to either wander, or go find an obj to watch
                //choose whether to use the item, or do something else
                int randomizeDecision = rand.nextInt(5);

                if (randomizeDecision < 2) {
                    ai_states = AI_STATES.WANDER;
                    return;
                }
                else {
                    ai_states = AI_STATES.DAILY_ROUTINE;
                    return;
                }
            }
        }
    }

    private float timer = 0.f;

    public void UseInteractable(float _dt)
    {
        float MAX_TIME_USE = 1.5f; //how long thr AI will stay at the location for
        timer += _dt;

        if (timer > MAX_TIME_USE) //change state
        {
            m_interactables.get(item).SetIsBeingUsed(false);

            Random random = new Random();
            int randomize = random.nextInt(8);

            if (randomize < 1)
                ai_states = AI_STATES.WANDER;
            else if (randomize < 3)
                ai_states = AI_STATES.DAILY_ROUTINE;
            else
                ai_states = AI_STATES.DEFAULT;

            timer = 0.f;
            return;
        }
        else
        {
            m_interactables.get(item).SetIsBeingUsed(true); //no money wasted

            if (!m_interactables.get(item).GetIsActive()) //if player off the appliance which AI still using, turn it back on
                m_interactables.get(item).SetActive(true);
        }
    }

    public void DailyRoutine(float _dt)
    {
        //find an on item
        if (isMoving)
            Movement(_dt);
        else
        {
            if (reachLocation)
            {
                ai_states = AI_STATES.USE_INTERACTBLE;
                reachLocation = false;
                return;
            }

            if (CheckEveryItemPowerInactive()) //make sure that there is at least one power on item
            {
                Random random = new Random();
                do {
                    item = random.nextInt(m_interactables.size()); //get one of the locations from the interactables
                } while (!m_interactables.get(item).GetIsActive());

                BFS(m_interactables.get(item).GetGridPos());
                isMoving = true;
            }
            else
            {
              //go back to default, or go to wander mode
                Random random = new Random();
                int randomize = random.nextInt(4);

                if (randomize < 3)
                    ai_states = AI_STATES.DEFAULT;
                else
                    ai_states = AI_STATES.WANDER;
            }
        }

    }

    private float stunTimer = 0.f;
    public void StunnedRoutine(float _dt)
    {
        float STUN_TIME = 2.f;
        stunTimer +=_dt;

        if (stunTimer > STUN_TIME)
        {
            //change state
            Random random = new Random();
            int randomize = random.nextInt(3);

            if (randomize == 0)
                ai_states = AI_STATES.DEFAULT;
            else if (randomize == 1)
                ai_states = AI_STATES.DAILY_ROUTINE;
            else if (randomize == 2)
                ai_states = AI_STATES.WANDER;

            stunTimer = 0.f;
        }
    }


    public void BFS(GridPt endPos)
    {
        gameData.SetStart(gridPos.x, gridPos.y); //set start position of BFS
        //get the nearest grid position to the object its going to

        gameData.SetEnd(endPos);
        gameData.BFS(gameData.m_start, gameData.m_end);
        for ( int i = 0; i < gameData.m_shortestPath.size(); ++i)
        {
            m_shortest.add(gameData.m_shortestPath.get(i));
        }
    }

    public void Movement(float _dt)
    {
        animation.Update(_dt); //update animation too
        // When pos is not the same as target
        if (dirMoved != MovingDir.NONE)
        {
            if (dirMoved == MovingDir.MOVEDOWN && position.y < target.y)
                position.y += _dt * 200;
            else if (dirMoved == MovingDir.MOVEDOWN && position.y >= target.y)
            {
                spritePic = downStaticSprite;
                dirMoved = MovingDir.NONE;
            }

            if (dirMoved == MovingDir.MOVEUP && position.y > target.y)
                position.y -= _dt * 200;
            else if (dirMoved == MovingDir.MOVEUP && position.y <= target.y)
            {
                spritePic = upStaticSprite;
                dirMoved = MovingDir.NONE;
            }

            if (dirMoved == MovingDir.MOVERIGHT && position.x < target.x)
                position.x += _dt * 200;
            else if (dirMoved == MovingDir.MOVERIGHT && position.x >= target.x)
            {
                spritePic = rightStaticSprite;
                dirMoved = MovingDir.NONE;
            }

            if (dirMoved == MovingDir.MOVELEFT && position.x > target.x)
                position.x -= _dt * 200;
            else if (dirMoved == MovingDir.MOVELEFT && position.x <= target.x) {
                spritePic = leftStaticSprite;
                dirMoved = MovingDir.NONE;
            }
        }
        else //move to the next position once it reach its the position its suppose to go
        {
            position = new Vector2(target.x, target.y);
            gridPos = gridTarget;
            m_shortest.remove(0); //remove the front of the shortest path

            //throw things
            Random random = new Random();
            if (random.nextInt(RUBBISH_DROPRATE) == 1)
            {
                Rubbish rubbish = new Rubbish(gridTarget, gameData);
                EntityManager.Instance.AddQueue(rubbish);
            }

            //MOVEMENT
            if (m_shortest.size() != 0) {
                if (m_shortest.get(0).x - gridTarget.x < 0)
                {
                    animation = leftWalkingAnim;
                    dirMoved = MovingDir.MOVELEFT;
                } else if (m_shortest.get(0).x - gridTarget.x > 0)
                {
                    animation = rightWalkingAnim;
                    dirMoved = MovingDir.MOVERIGHT;
                } else if (m_shortest.get(0).y - gridTarget.y < 0)
                {
                    animation = downWalkingAnim;
                    dirMoved = MovingDir.MOVEDOWN;
                } else if (m_shortest.get(0).y - gridTarget.y > 0)
                {
                    animation = upWalkingAnim;
                    dirMoved = MovingDir.MOVEUP;
                } else
                    dirMoved = MovingDir.NONE;

                target = new Vector2(m_shortest.get(0).x * m_gridSizeX + m_gridOffsetX, (gameData.grid.m_sizeY - 1 - m_shortest.get(0).y) * m_gridSizeY + m_gridOffsetY);
                gridTarget.Set(m_shortest.get(0).x, m_shortest.get(0).y);

                CheckCollide(); //check collision
            }
            else
            {
                reachLocation = true;
                isMoving = false;
            }
        }
    }


    public boolean CheckEveryItemPower() //make sure there are some appliances that still have not be turned on
    {
        for (int i =0; i < m_interactables.size(); ++i) {
            EntityBase currEntity = m_interactables.get(i);

            if (!currEntity.GetIsActive() && (currEntity.GetType() != EntityTypes.DEFAULT && currEntity.GetType() != EntityTypes.RUBBISH))
                return true; //at least one appliance is still not active

        }
        return false;
    }

    public boolean CheckEveryItemPowerInactive() //make sure there are some appliances that still have not be turned on
    {
        for (int i =0; i < m_interactables.size(); ++i) {
            EntityBase currEntity = m_interactables.get(i);

            if (currEntity.GetIsActive() && (currEntity.GetType() != EntityTypes.DEFAULT && currEntity.GetType() != EntityTypes.RUBBISH))
                return true; //at least one appliance is still active

        }
        return false; //all not active
    }


    public boolean CheckCollide()
    {
        if (gridPos.isEqual(Player.Instance.GetGridPos())|| gridPos.isEqual(Player.Instance.GetGridTarget()) || gridTarget.isEqual(Player.Instance.GetGridTarget()) || gridTarget.isEqual(Player.Instance.GetGridPos())) //if collide with the player
        {
            isMoving = false;
            gridTarget = gridPos;
            m_shortest.clear();
            before_stun_state = ai_states;
            ai_states = AI_STATES.STUNNED;
            return true;
        }

        return false;
    }

    public AI_STATES GetCurrState() { return ai_states; }

    @Override
    public float GetRadius()
    {
        return animation.GetWidth() /2;
    }

    @Override
    public Vector2 GetPosMin() {
        return new Vector2(position.x - animation.GetWidth()/2,position.y - animation.GetHeight()/2);
    }

    @Override
    public Vector2 GetPosMax()
    {
        return new Vector2(position.x + animation.GetWidth()/2 ,position.y + animation.GetHeight() /2);
    }

    @Override
    public int GetRenderLayer()
    {
        return LayerConstants.MOVING_GAMEOBJECTS_LAYER;
    }

    @Override
    public void SetRenderLayer(int _newLayout) { return; }

    public void SetInteractables(Vector<EntityBase> locations) {m_interactables = locations; }
}
